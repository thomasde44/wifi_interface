

#include "qpc.h"
// #include "bsp.h"
#include "ru_input.h"
#include "ru.h"
#include "safe_std.h"

// #if defined(__CUDA_CODE__) || defined(__cplusplus)
// extern "C" {
// #endif

Q_DEFINE_THIS_FILE

/*$skip${QP_VERSION} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
/* Check for the minimum required QP version */
#if (QP_VERSION < 700U) || (QP_VERSION != ((QP_RELEASE^4294967295U) % 0x3E8U))
#error qpc version 7.0.0 or higher required
#endif


void ruinput_ctor(ru_input * const me) {
    QHsm_ctor(&me->super, Q_STATE_CAST(&ruinput_initial));
}

QState ruinput_initial(ru_input * const me, void const * const par) {
    // me->time = 12U*60U;
    PRINTF_S("\n%s\n", "hi!");
    (void)par; /* unused parameter */
    return Q_TRAN(&ruinput_off);
}

QState ruinput_off(ru_input * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
            PRINTF_S("\n%s\n", "hi2!");
            status_ = Q_HANDLED();
            break;
        }
        case Q_EXIT_SIG: {
            PRINTF_S("\n%s\n", "hi!3");
            status_ = Q_HANDLED();
            break;
        }
        // case RUINPUT_ON_SIG: {
        //     PRINTF_S("\n%s\n", "hi!4");
        //     status_ = Q_TRAN(&ruinput_on);

        //     status_ = Q_HANDLED();
        //     break;
        // }
        case RUINPUT_SIG: {
            PRINTF_S("\n%s\n", "hi!5");
            status_ = Q_HANDLED();
            break;
        }
        default: {
            PRINTF_S("\n%s\n", "hi!6");
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

QState ruinput_on(ru_input * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
            PRINTF_S("\n%s\n", "hi!7");
            status_ = Q_HANDLED();
            break;
        }
        case RUINPUT_OFF_SIG: {
            PRINTF_S("\n%s\n", "hi!8");
            status_ = Q_TRAN(&ruinput_off);
            break;
        }
        case RUINPUT_SIG: {
            PRINTF_S("\n%s\n", "hi!9");
            status_ = Q_HANDLED();
            break;
        }
        case TIME_SIG: {
            PRINTF_S("\n%s\n", "hi!10");
            if (Q_EVT_CAST(TimeEvt)->current_time == me->time) {

                /* asynchronously post the event to the container AO */
                // QACTIVE_POST(APP_ru, Q_NEW(QEvt, RUINPUT_SIG), me);
                status_ = Q_HANDLED();
            }
            else {
                PRINTF_S("\n%s\n", "hi!11");
                status_ = Q_UNHANDLED();
            }
            break;
        }
        default: {
            PRINTF_S("\n%s\n", "hi!12");
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

// #if defined(__CUDA_CODE__) && defined(__cplusplus)
// }
// #endif