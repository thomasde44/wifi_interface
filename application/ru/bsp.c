#include "qpc.h"
#include "ru.h"
#include "bsp.h"

#include "safe_std.h" /* portable "safe" <stdio.h>/<string.h> facilities */
#include <stdlib.h>   /* for exit() */



Q_DEFINE_THIS_FILE



/*..........................................................................*/
void QF_onStartup(void) {
    QF_setTickRate(BSP_TICKS_PER_SEC, 30); /* set the desired tick rate */
    QF_consoleSetup();
}
/*..........................................................................*/
void QF_onCleanup(void) {
    PRINTF_S("\n%s\n", "Bye! Bye!");
    QF_consoleCleanup();
}
/*..........................................................................*/
void QF_onClockTick(void) {
    QTIMEEVT_TICK_X(0U, &l_clock_tick); /* perform the QF clock tick processing */
    // int key = QF_consoleGetKey();
    // if (key != 0) { /* any key pressed? */
    //     BSP_onKeyboardInput((uint8_t)key);
    // }
}

/*..........................................................................*/
Q_NORETURN Q_onAssert(char const * const file, int_t const line) {
    FPRINTF_S(stderr, "Assertion failed in %s, line %d", file, line);
    exit(-1);
}

