#include "qpc.h"
#include "ru_input.h"
#include "ru.h"
#include <unistd.h>
// #include "bsp.h"

#include "safe_std.h" /* portable "safe" <stdio.h>/<string.h> facilities */

Q_DEFINE_THIS_FILE

/*..........................................................................*/
int main() {
    /* queue storage for ru */
    static QEvt const *l_ruQSto[10];
    /* storage for small pool */
    static QF_MPOOL_EL(TimeEvt) l_smlPoolSto[10];



    QF_init(); /* initialize the framework and the underlying RT kernel */

    /* publish-subscribe not used, no call to QF_psInit() */

    /* initialize event pools... */
    QF_poolInit(l_ruQSto, sizeof(l_ruQSto), sizeof(l_ruQSto[0]));

    /* instantiate and start the active objects... */
    ru_ctor();
    QACTIVE_START(APP_ru, 1U,
                  l_ruQSto, Q_DIM(l_ruQSto),
                  (void *)0, 0U, (QEvt *)0);

    return QF_run(); /* run the QF application */
}