#ifndef RU_INPUT_H
#define RU_INPUT_H
#include <sys/types.h>

// #if defined(__CUDA_CODE__) && defined(__cplusplus)
// extern "C" {
// #endif


typedef struct {
    QHsm super;
    ssize_t nbytes_rx;
    char * p_dat;
    uint32_t time;
} ru_input;

/* public: */
void ruinput_ctor(ru_input * const me);

/* protected: */
QState ruinput_initial(ru_input * const me, void const * const par);
QState ruinput_off(ru_input * const me, QEvt const * const e);
QState ruinput_on(ru_input * const me, QEvt const * const e);

// #if defined(__CUDA_CODE__) && defined(__cplusplus)
// }
// #endif

#endif /* RU_INPUT_H */