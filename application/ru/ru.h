#ifndef RU_H
#define RU_H

#if defined(__CUDA_CODE__) && defined(__cplusplus)
extern "C" {
#endif

#include <sys/types.h>



// (enum QReservedSig)Q_USER_SIG = 4

enum ru_singals {
    TICK_SIG = Q_USER_SIG,
    RUINPUT_SET_SIG,
    RUINPUT_ON_SIG,
    RUINPUT_OFF_SIG,
    RUINPUT_SIG,
    RU_HALF_SIG,
    RU_FULL_SIG,
    TIME_SIG,
    TERMINATE_SIG,
    IGNORE_SIG,
    MAX_SIG
};



typedef struct {
/* protected: */
    QEvt super;
    uint32_t count;
/* public: */
    uint32_t current_time;
} TimeEvt;

typedef struct {
/* protected: */
    QEvt super;
    uint32_t count;
/* public: */
    uint32_t current_time;
} TimeEvt2;

// typedef struct {

//   QActive super;
//   ssize_t nbytes_rx;
//   char * p_dat;
//   TimeEvt timeEvt;
// } ru;



extern QActive * const APP_ru;


void ru_ctor(void);

#if defined(__CUDA_CODE__) && defined(__cplusplus)
}
#endif

#endif /* RU_H */