// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * libiio - AD9361 IIO streaming example
 *
 * Copyright (C) 2014 IABG mbH
 * Author: Michael Feilen <feilen_at_iabg.de>
 **/

#include <stdbool.h>
#include <stdint.h>
// #include <string.h>
#include <signal.h>
// #include <stdio.h>
#include <iio.h>
#include <time.h>
#include "shared_mem.h"
#if defined(__CUDA_CODE__) || defined(__cplusplus)
extern "C" {
#endif

/* helper macros */
#define MHZ(x) ((long long)(x*1000000.0 + .5))
#define GHZ(x) ((long long)(x*1000000000.0 + .5))

#define IIO_ENSURE(expr) { \
	if (!(expr)) { \
		(void) fprintf(stderr, "assertion failed (%s:%d)\n", __FILE__, __LINE__); \
		(void) abort(); \
	} \
}

/* RX is input, TX is output */
enum iodev { RX, TX };

/* common RX and TX streaming params */
struct stream_cfg {
	long long bw_hz; // Analog banwidth in Hz
	long long fs_hz; // Baseband sample rate in Hz
	long long lo_hz; // Local oscillator frequency in Hz
	const char* rfport; // Port name
};

/* static scratch mem for strings */
static char tmpstr[64];

/* IIO structs required for streaming */
static struct iio_context *ctx   = NULL;
static struct iio_channel *rx0_i = NULL;
static struct iio_channel *rx0_q = NULL;
static struct iio_channel *tx0_i = NULL;
static struct iio_channel *tx0_q = NULL;
static struct iio_buffer  *rxbuf = NULL;
static struct iio_buffer  *txbuf = NULL;

static bool stop;

/* cleanup and exit */
static void shutdown()
{
	printf("* Destroying buffers\n");
	if (rxbuf) { iio_buffer_destroy(rxbuf); }
	if (txbuf) { iio_buffer_destroy(txbuf); }

	printf("* Disabling streaming channels\n");
	if (rx0_i) { iio_channel_disable(rx0_i); }
	if (rx0_q) { iio_channel_disable(rx0_q); }
	if (tx0_i) { iio_channel_disable(tx0_i); }
	if (tx0_q) { iio_channel_disable(tx0_q); }

	printf("* Destroying context\n");
	if (ctx) { iio_context_destroy(ctx); }
	exit(0);
}

static void handle_sig(int sig)
{
	printf("Waiting for process to finish... Got signal %d\n", sig);
	stop = true;
}

/* check return value of attr_write function */
static void errchk(int v, const char* what) {
	 if (v < 0) { fprintf(stderr, "Error %d writing to channel \"%s\"\nvalue may not be supported.\n", v, what); shutdown(); }
}

/* write attribute: long long int */
static void wr_ch_lli(struct iio_channel *chn, const char* what, long long val)
{
	errchk(iio_channel_attr_write_longlong(chn, what, val), what);
}

/* write attribute: string */
static void wr_ch_str(struct iio_channel *chn, const char* what, const char* str)
{
	errchk(iio_channel_attr_write(chn, what, str), what);
}

/* helper function generating channel names */
static char* get_ch_name(const char* type, int id)
{
	snprintf(tmpstr, sizeof(tmpstr), "%s%d", type, id);
	return tmpstr;
}

/* returns ad9361 phy device */
static struct iio_device* get_ad9361_phy(void)
{
	struct iio_device *dev =  iio_context_find_device(ctx, "ad9361-phy");
	IIO_ENSURE(dev && "No ad9361-phy found");
	return dev;
}

/* finds AD9361 streaming IIO devices */
static bool get_ad9361_stream_dev(enum iodev d, struct iio_device **dev)
{
	switch (d) {
	case TX: *dev = iio_context_find_device(ctx, "cf-ad9361-dds-core-lpc"); return *dev != NULL;
	case RX: *dev = iio_context_find_device(ctx, "cf-ad9361-lpc");  return *dev != NULL;
	default: IIO_ENSURE(0); return false;
	}
}

/* finds AD9361 streaming IIO channels */
static bool get_ad9361_stream_ch(enum iodev d, struct iio_device *dev, int chid, struct iio_channel **chn)
{
	*chn = iio_device_find_channel(dev, get_ch_name("voltage", chid), d == TX);
	if (!*chn)
		*chn = iio_device_find_channel(dev, get_ch_name("altvoltage", chid), d == TX);
	return *chn != NULL;
}

/* finds AD9361 phy IIO configuration channel with id chid */
static bool get_phy_chan(enum iodev d, int chid, struct iio_channel **chn)
{
	switch (d) {
	case RX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name("voltage", chid), false); return *chn != NULL;
	case TX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name("voltage", chid), true);  return *chn != NULL;
	default: IIO_ENSURE(0); return false;
	}
}

/* finds AD9361 local oscillator IIO configuration channels */
static bool get_lo_chan(enum iodev d, struct iio_channel **chn)
{
	switch (d) {
	 // LO chan is always output, i.e. true
	case RX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name("altvoltage", 0), true); return *chn != NULL;
	case TX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name("altvoltage", 1), true); return *chn != NULL;
	default: IIO_ENSURE(0); return false;
	}
}

/* applies streaming configuration through IIO */
bool cfg_ad9361_streaming_ch(struct stream_cfg *cfg, enum iodev type, int chid)
{
	struct iio_channel *chn = NULL;

	// Configure phy and lo channels
	printf("* Acquiring AD9361 phy channel %d\n", chid);
	if (!get_phy_chan(type, chid, &chn)) {	return false; }
	wr_ch_str(chn, "rf_port_select",     cfg->rfport);
	wr_ch_lli(chn, "rf_bandwidth",       cfg->bw_hz);
	wr_ch_lli(chn, "sampling_frequency", cfg->fs_hz);

	// Configure LO channel
	printf("* Acquiring AD9361 %s lo channel\n", type == TX ? "TX" : "RX");
	if (!get_lo_chan(type, &chn)) { return false; }
	wr_ch_lli(chn, "frequency", cfg->lo_hz);
	return true;
}

/* simple configuration and streaming */
/* usage:
 * Default context, assuming local IIO devices, i.e., this script is run on ADALM-Pluto for example
 $./a.out
 * URI context, find out the uri by typing `iio_info -s` at the command line of the host PC
 $./a.out usb:x.x.x (old instructions)

 	rx_bw_mhz = 2
	rx_sample_rate_mhz = 2.5
	rx_lo_frq_mhz = 2.5
	tx_bw_mhz = 1.5
	tx_sample_rate_mhz 2.5
	tx_lo_frq_mhz 2.5
 
 
 */



void get_burst(
					  size_t nrx, 
					  size_t prev_nrx, 
					  struct iio_buffer * rxbuf, 
					  struct iio_channel * rx0_i,
					  struct iio_device * rx
					  ) {
	// start_time = clock();
	ssize_t nbytes_rx;
	void *p_dat, *p_end;
	ptrdiff_t p_inc;

	// Refill RX buffer
	nbytes_rx = iio_buffer_refill(rxbuf);
	if (nbytes_rx < 0) { printf("Error refilling buf %d\n",(int) nbytes_rx); shutdown(); }

	// READ: Get pointers to RX buf and read IQ from RX buf port 0
	// p_inc = iio_buffer_step(rxbuf);
	// p_end = iio_buffer_end(rxbuf);
    // // printf("step size: %u\n", (uint32_t)p_inc);
	// for (p_dat = (char *)iio_buffer_first(rxbuf, rx0_i); p_dat < p_end; p_dat += p_inc) {
	// 	// Example: swap I and Q
	// 	const int16_t i = ((int16_t*)p_dat)[0]; // Real (I)
	// 	const int16_t q = ((int16_t*)p_dat)[1]; // Imag (Q)
	// 	((int16_t*)p_dat)[0] = q;
	// 	((int16_t*)p_dat)[1] = i;
	// }
	p_dat = iio_buffer_first(rxbuf, rx0_i);
	printf("iio_device_get_sample_size %lu\n", iio_device_get_sample_size(rx));
	nrx += nbytes_rx / iio_device_get_sample_size(rx);
	printf("\tRX %8.2f MSmp accumulated prev rx sample amount %8.2ld \n", nrx/1e6, (nrx - prev_nrx));
    // printf("\tRX %8.2d MSmp, TX %8.2d MSmp\n", nrx/1e6, ntx/1e6);
    prev_nrx = nrx;
	char name[] = "shm1";
	post_to_shm(name, p_dat, nrx);
	

	// ((int16_t*)p_dat)[0] = q;
	// ((int16_t*)p_dat)[1] = i;
	// return p_dat;


}



int stream_iq (
	long long rx_bw_mhz, //2
	long long rx_lo_frq_ghz, //2.5
	long long tx_bw_mhz, //1.5
	long long tx_lo_frq_ghz) //2.5
{
	// Streaming devices
	struct iio_device *tx;
	struct iio_device *rx;

	// RX and TX sample counters
	size_t nrx = 0;
	size_t ntx = 0;
    size_t prev_nrx = 0;
	size_t prev_ntx = 0;

	// Stream configurations
	struct stream_cfg rxcfg;
	struct stream_cfg txcfg;

	// Listen to ctrl+c and IIO_ENSURE
	// signal(SIGINT, handle_sig);

	// RX stream config
	rxcfg.bw_hz = MHZ((long long)rx_bw_mhz);   // 2 MHz rf bandwidth
	rxcfg.fs_hz = MHZ(2.5);   // 2.5 MS/s rx sample rate
	rxcfg.lo_hz = MHZ((long long)98.5); // 2.5 GHz rf frequency
	rxcfg.rfport = "A_BALANCED"; // port A (select for rf freq.)

	// TX stream config
	txcfg.bw_hz = MHZ((long long)tx_bw_mhz); // 1.5 MHz rf bandwidth
	txcfg.fs_hz = MHZ(2.5);   // 2.5 MS/s tx sample rate
	txcfg.lo_hz = GHZ((long long)tx_lo_frq_ghz); // 2.5 GHz rf frequency
	txcfg.rfport = "A"; // port A (select for rf freq.)
	// printf("* %s\n", argv[1]);
	printf("* Acquiring IIO context\n");

	IIO_ENSURE((ctx = iio_create_context_from_uri("usb:3.5.5")) && "No context");


	IIO_ENSURE(iio_context_get_devices_count(ctx) > 0 && "No devices");

	printf("* Acquiring AD9361 streaming devices\n");
	IIO_ENSURE(get_ad9361_stream_dev(TX, &tx) && "No tx dev found");
	IIO_ENSURE(get_ad9361_stream_dev(RX, &rx) && "No rx dev found");

	printf("* Configuring AD9361 for streaming\n");
	IIO_ENSURE(cfg_ad9361_streaming_ch(&rxcfg, RX, 0) && "RX port 0 not found");
	IIO_ENSURE(cfg_ad9361_streaming_ch(&txcfg, TX, 0) && "TX port 0 not found");

	printf("* Initializing AD9361 IIO streaming channels\n");
	IIO_ENSURE(get_ad9361_stream_ch(RX, rx, 0, &rx0_i) && "RX chan i not found");
	IIO_ENSURE(get_ad9361_stream_ch(RX, rx, 1, &rx0_q) && "RX chan q not found");
	IIO_ENSURE(get_ad9361_stream_ch(TX, tx, 0, &tx0_i) && "TX chan i not found");
	IIO_ENSURE(get_ad9361_stream_ch(TX, tx, 1, &tx0_q) && "TX chan q not found");

	printf("* Enabling IIO streaming channels\n");
	iio_channel_enable(rx0_i);
	iio_channel_enable(rx0_q);
	iio_channel_enable(tx0_i);
	iio_channel_enable(tx0_q);

	printf("* Creating non-cyclic IIO buffers with 1 MiS\n");
	rxbuf = iio_device_create_buffer(rx, 2084000, false);
	if (!rxbuf) {
		perror("Could not create RX buffer");
		shutdown();
	}
	txbuf = iio_device_create_buffer(tx, 2*1024*1024, false);
	if (!txbuf) {
		perror("Could not create TX buffer");
		shutdown();
	}
	// for timing
	clock_t start_time, end_time;
    double execution_time;
	printf("* Starting IO streaming (press CTRL+C to cancel)\n");
	char name[] = "shm1";
	struct shmbuf_iq * shm_a = call_shared_memory(name);
	shm_a->data_id = 0;

	while (!stop)
	{
		start_time = clock();
		// ((int16_t*)ret_buf)[0] = q;
		// ((int16_t*)ret_buf)[1] = i;
		char * ret_buf;
		get_burst(nrx, prev_nrx, rxbuf, rx0_i, rx);
		end_time = clock();
		execution_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
		printf("Execution time: %f seconds\n", execution_time);
	}
	shutdown();
	return 0;
}


int main() {
	stream_iq(10, 98.5, 1.5, 2.5);
}


#if defined(__CUDA_CODE__) && defined(__cplusplus)
}
#endif
// 1048576