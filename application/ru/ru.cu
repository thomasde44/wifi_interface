#if defined(__CUDA_CODE__) || defined(__cplusplus)
extern "C" {
#endif

#include "qpc.h"
#include "safe_std.h"
#include <stdlib.h>
#include "shared_mem.h"
#include <signal.h>
#include "ru_input.h"
#include "ru.h"
#include "crc24a.h"
#include "fft_utils.h"
#include <cuda_runtime.h>
#include <cufft.h>
#include <cufftXt.h>


 /* portable "safe" <stdio.h>/<string.h> facilities */



Q_DEFINE_THIS_FILE

/* Active object class -----------------------------------------------------*/

typedef struct {
/* protected: */
    QActive super;

/* private: */
    uint32_t current_time;
    ru_input input;

/* public: */
    QTimeEvt timeEvt;
    QTimeEvt timeEvt2;
} ru;

/* protected: */
static QState ru_initial(ru * const me, void const * const par);
static QState ru_handle_input(ru * const me, QEvt const * const e);

static cufftHandle * plan;
// static QState ru_full(ru * const me, QEvt const * const e);
// static QState ru_half(ru * const me, QEvt const * const e);
// static QState ru_final(ru * const me, QEvt const * const e);

/* Local objects -----------------------------------------------------------*/
static ru l_ru; /* the single inst of the AO */

/* Global-scope objects ----------------------------------------------------*/
#if (QP_VERSION < 700U) || (QP_VERSION != ((QP_RELEASE^4294967295U) % 0x3E8U))
#error qpc version 7.0.0 or higher required
#endif

static bool stop;
static void handle_sig(int sig)
{
	printf("Waiting for process to finish... Got signal %d\n", sig);
	stop = true;
}

extern QActive * const APP_ru = &l_ru.super;

void ru_ctor(void) {
    ru * const me = &l_ru;

    QActive_ctor(&me->super, Q_STATE_CAST(&ru_initial));
    ruinput_ctor(&me->input); /* orthogonal component ctor */
    /* private time event ctor */
    QTimeEvt_ctorX(&me->timeEvt, &me->super, RUINPUT_SIG, 0U);
    // can arm new timers here to trigger periodic events to play out here
    // or can dispatch events to ru_input state machine as well
    // QTimeEvt_ctorX(&me->timeEvt2, &me->super, RUINPUT_ON_SIG, 0U);
}

static QState ru_initial(ru * const me, void const * const par) {
    (void)par; /* unused parameter */
    me->current_time = 0U;

    /* (!) trigger the initial transition in the component */
    QHSM_INIT((QHsm *)&me->input, (void *)0, me->super.prio);
    return Q_TRAN(&ru_handle_input);
    signal(SIGINT, handle_sig);
}

static QState ru_handle_input(ru * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
            PRINTF_S("\n%s \n", "hi! hi!");
            TimeEvt pe;
            /* periodic timeout every second  send TICK_SIG at this period*/
            QTimeEvt_armX(&me->timeEvt, 8U,
                                        8U);
                                        
            // QTimeEvt_armX(&me->timeEvt2, 30U,
            //                             30U);
            // while(!stop) {
            //     PRINTF_S("\n%s \n", "fugg");
            //     me->current_time += 0.5;
            //     QHSM_DISPATCH(&me->input.super, &pe.super, me->super.prio);
            // }

            status_ = Q_HANDLED();
            break;
        }
        case Q_EXIT_SIG: {
            PRINTF_S("\n%s\n", "hi! hi!2");
            QTimeEvt_disarm(&me->timeEvt);
            QTimeEvt_disarm(&me->timeEvt2);
            status_ = Q_HANDLED();
            break;
        }


        // read from shared memory on expiry of every alarm signal 
        case RUINPUT_SIG: {
            PRINTF_S("\n%s\n", "hi! hi!6");
            me->current_time += 1;

            char name[] = "shm1";
            struct shmbuf_iq * shm_a = call_shared_memory(name);
            PRINTF_S("data: %i %li\n", shm_a->data_id, shm_a->data_size);
            
            
            do_cufft_plan(plan, 368640, 2048);
            // calculateCRC24(shm_a->data, shm_a->data_size);
            // BSP_showMsg("Wake up!!!");
            // if (me->current_time > 8) {
            //     QHSM_DISPATCH((QHsm *)&me->input, e, me->super.prio);
            //     QACTIVE_POST(APP_ru, Q_NEW(QEvt, Q_EXIT_SIG), me);
            // }
            status_ = Q_HANDLED();
            break;
        }
        case RUINPUT_SET_SIG: /* intentionally fall through */
        case RUINPUT_ON_SIG: /* intentionally fall through */
        case RUINPUT_OFF_SIG: {
            PRINTF_S("\n%s\n", "hi! hi!7");
            /* (!) synchronously dispatch to the orthogonal component */
            QHSM_DISPATCH((QHsm *)&me->input, e, me->super.prio);
            status_ = Q_HANDLED();
            exit(1);
            break;
        }

        default: {
            PRINTF_S("\n%s\n", "hi! hi!9");
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

#if defined(__CUDA_CODE__) || defined(__cplusplus)
}
#endif