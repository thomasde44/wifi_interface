// includes, system
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// includes, project
#include <cuda_runtime.h>
#include <cufft.h>
#include <cufftXt.h>

#include "helper_cuda.h"
#if defined(__CUDA_CODE__) || defined(__cplusplus)
extern "C" {
#endif

const int vector_size = 2048;


void processComplexData(const uint8_t* inputBuffer, size_t numElements, float2* outputBuffer) {
    for (size_t i = 0; i < numElements; ++i) {
        // Interpret the 16-bit complex number data from the input buffer
        uint16_t* complexData = (uint16_t*)(inputBuffer + (i * 4)); // Assuming little-endian byte order

        // Extract the real and imaginary parts
        uint16_t realPart = complexData[0];
        uint16_t imagPart = complexData[1];

        // Convert to floating-point values and store in the output buffer
        outputBuffer[i].x = (float)realPart;
        outputBuffer[i].y = (float)imagPart;
    }
}


void do_cufft_plan(cufftHandle * plan, ssize_t size, ssize_t stride){
    if (checkCudaErrors(cufftPlan1d(plan, size, CUFFT_C2C, stride))) {
        printf("cufftPlan1d ok\n");
    } else
    {
        printf("cufftPlan1d failed\n");
        exit(1);
    }
}

void do_cufft(Handle * plan, uint8_t * data, int data_size, int threads, int blocks) {
    cudaError_t cudaStatus;
    float2 * d_input;
    
    static dim3 threads_per_block(threads, 1);
    static dim3 blocks_per_grid(blocks,1);

    cudaStatus = cudaMalloc(reinterpret_cast<void **>(&d_input),blocks*threads*sizeof(float2));
    if (cudaStatus != cudaSuccess) {
        printf("cudaMalloc failed: %s\n", cudaGetErrorString(cudaStatus));
        // Handle the error or clean up resources
    }
    cudaStatus = cudaMemcpy(d_input, data, threads*blocks*sizeof(float2),cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        printf("cudaMalloc failed: %s\n", cudaGetErrorString(cudaStatus));
        // Handle the error or clean up resources
    }
}



__global__ void memcpy_kernel(int **in, int **out, int len, int N)
{
    int idx = threadIdx.x + blockIdx.x*blockDim.x;

    for(; idx<N; idx+=gridDim.x*blockDim.x)
        memcpy(out[idx], in[idx], sizeof(int)*len);

}

#if defined(__CUDA_CODE__) && defined(__cplusplus)
}
#endif