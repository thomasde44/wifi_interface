#include <sys/mman.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if defined(__CUDA_CODE__) || defined(__cplusplus)
extern "C" {
#endif



#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \
                        } while (0)

/* Define a structure that will be imposed on the shared
    memory object */

sem_t mutex;


// stream iq data from the transceiver into here
// asynchronously read the data and process in the quantum application
struct shmbuf_iq {

    int thread_id;
    int data_id;
    ssize_t data_size;
    uint8_t * data;
};



// ----------------------------------------------
// C++ version
// ----------------------------------------------
// returns file desfriptor of shared memory
// shmbuf_iq * call_shared_memory(std::string name) {

//     int fd = shm_open(name.c_str(), O_CREAT | O_RDWR, 0666);

//     if (fd == -1) {
//         errExit("shm_open");
//     }

//     if (ftruncate(fd, sizeof(struct shmbuf_iq)) == -1) {
//         errExit("ftruncate");
//     }

//     struct shmbuf_iq *shmp = (shmbuf_iq*)mmap(NULL, sizeof(*shmp), PROT_READ | PROT_WRITE,
//                                           MAP_SHARED, fd, 0);
    
//     if (shmp == MAP_FAILED) {
//         errExit("mmap");
//     }
    

//     return shmp;        

// }


// -----------------------------------------------------------------
// use index string to load shared memory segment
// return the retreived buffer
// C version
// -----------------------------------------------------------------
struct shmbuf_iq * call_shared_memory(const char *name) {
    int fd = shm_open(name, O_CREAT | O_RDWR, 0666);
    if (fd == -1) {
        // Handle the error condition accordingly
        return NULL;
    }

    if (ftruncate(fd, sizeof(struct shmbuf_iq)) == -1) {
        // Handle the error condition accordingly
        close(fd);
        return NULL;
    }

    struct shmbuf_iq *shmp = (struct shmbuf_iq *)mmap(NULL, sizeof(*shmp), PROT_READ | PROT_WRITE,
                                                      MAP_SHARED, fd, 0);

    if (shmp == MAP_FAILED) {
        // Handle the error condition accordingly
        close(fd);
        return NULL;
    }

    close(fd);
    return shmp;
}

sem_t *call_semaphore(const char *name) {
    sem_t *sem = sem_open(name, O_CREAT | O_RDWR, 0666, 1);

    if (sem == SEM_FAILED) {
        // Handle the error condition accordingly
        printf("SEM_FAILED\n");
        return NULL;
    }

    return sem;
}

// ----------------------------------------------
// C++ version
// ----------------------------------------------
// void * post_to_shm(void * ptr) {

//     std::string * info = (std::string *) (ptr);

//     int report_cnt = 0;

//     shmbuf_iq * shmp = call_shared_memory(info[3]);

//     int sleep_interval = std::stoi(info[2]);
//     int tid = std::stoi(info[0]);
//     // std::cout << "thread id: " << shmp->thread_id << std::endl;
//     while (1) {
//         int value;

//         sem_post(&mutex);
//         report_cnt += 1;
//         shmp->report_id = report_cnt;
//         shmp->thread_id = tid;
//         shmp->time_interval = sleep_interval;
//         std::cout << shmp->report_id << " " << shmp->thread_id << " " << shmp->time_interval << std::endl;
//         sleep(sleep_interval);
//         sem_wait(&mutex);
//     }

// }

// ----------------------------------------------
// C version
// ----------------------------------------------
void *post_to_shm(void *ptr, char * data, ssize_t size) {
    // string casting
    char *info = (char *)ptr;


    struct shmbuf_iq *shmp = call_shared_memory(info);

    shmp->data = (uint8_t *)malloc(size);
    if (shmp->data == NULL) {
        // Handle memory allocation error
        printf("mem alloc error\n");
    }

    // int value;
    sem_post(&mutex);

    shmp->data_id++;
    shmp->data_size = size;

    memcpy(shmp->data, (uint8_t *)data, size);

    sem_trywait(&mutex);
    // }
}


#if defined(__CUDA_CODE__) && defined(__cplusplus)
}
#endif