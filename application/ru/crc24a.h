#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#if defined(__CUDA_CODE__) || defined(__cplusplus)
extern "C" {
#endif

// CRC generator polynomial
#define CRC_POLYNOMIAL 0x1864CFB

// Calculate CRC using the given generator polynomial
uint32_t calculateCRC24(const char* data, size_t size) {
    uint32_t crc = 0xFFFFFFFF;

    for (size_t i = 0; i < size; i++) {
        crc ^= data[i];

        for (int j = 0; j < 8; j++) {
            if (crc & 0x80000000) {
                crc = (crc << 1) ^ CRC_POLYNOMIAL;
            } else {
                crc <<= 1;
            }
        }
    }

    return crc;
}

#if defined(__CUDA_CODE__) && defined(__cplusplus)
}
#endif