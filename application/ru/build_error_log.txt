nvcc -MM -MT build/ru_input.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ru_input.c > build/ru_input.d
nvcc -MM -MT build/ru.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ru.cu > build/ru.d
nvcc -MM -MT build/qf_port.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//ports/posix-qv/qf_port.c > build/qf_port.d
nvcc -MM -MT build/qf_time.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_time.c > build/qf_time.d
nvcc -MM -MT build/qf_qmact.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_qmact.c > build/qf_qmact.d
nvcc -MM -MT build/qf_qeq.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_qeq.c > build/qf_qeq.d
nvcc -MM -MT build/qf_qact.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_qact.c > build/qf_qact.d
nvcc -MM -MT build/qf_ps.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_ps.c > build/qf_ps.d
nvcc -MM -MT build/qf_mem.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_mem.c > build/qf_mem.d
nvcc -MM -MT build/qf_dyn.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_dyn.c > build/qf_dyn.d
nvcc -MM -MT build/qf_defer.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_defer.c > build/qf_defer.d
nvcc -MM -MT build/qf_actq.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_actq.c > build/qf_actq.d
nvcc -MM -MT build/qf_act.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_act.c > build/qf_act.d
nvcc -MM -MT build/qep_msm.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qep_msm.c > build/qep_msm.d
nvcc -MM -MT build/qep_hsm.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qep_hsm.c > build/qep_hsm.d
nvcc -MM -MT build/bsp.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 bsp.c > build/bsp.d
nvcc -MM -MT build/main.o -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 main.c > build/main.d
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 main.c -o build/main.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 bsp.c -o build/bsp.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qep_hsm.c -o build/qep_hsm.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qep_msm.c -o build/qep_msm.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_act.c -o build/qf_act.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_actq.c -o build/qf_actq.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_defer.c -o build/qf_defer.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_dyn.c -o build/qf_dyn.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_mem.c -o build/qf_mem.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_ps.c -o build/qf_ps.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_qact.c -o build/qf_qact.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_qeq.c -o build/qf_qeq.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_qmact.c -o build/qf_qmact.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//src/qf/qf_time.c -o build/qf_time.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//ports/posix-qv/qf_port.c -o build/qf_port.o
nvcc -D__CUDA_CODE__ -c -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ru_input.c -o build/ru_input.o
nvcc -c -g -G -m64 -std=c++11 --cudart=shared -I.  -I..//include -I..//ports/posix-qv -DQP_API_VERSION=9999 ..//include/qstamp.c -o build/qstamp.o
nvcc   -o build/ru build/main.o build/bsp.o build/qep_hsm.o build/qep_msm.o build/qf_act.o build/qf_actq.o build/qf_defer.o build/qf_dyn.o build/qf_mem.o build/qf_ps.o build/qf_qact.o build/qf_qeq.o build/qf_qmact.o build/qf_time.o build/qf_port.o build/ru.o build/ru_input.o build/qstamp.o -lpthread -lrt -lcufft 
nvlink warning : Skipping incompatible '/usr/lib/x86_64-linux-gnu/libpthread.a' when searching for -lpthread
nvlink warning : Skipping incompatible '/usr/lib/x86_64-linux-gnu/librt.a' when searching for -lrt
nvlink fatal   : Could not open input file 'build/ru.o'
make: *** [Makefile:206: build/ru] Error 1