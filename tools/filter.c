#ifndef FILTER_H
#define FILTER_H

#include <math.h>
#include <stdio.h>
#include <liquid/liquid.h>
// #include <complex>
// #include <liquid/liquid.internal.h>
// Function: double j0 (double x)
// Function: float j0f (float x)
// Function: long double j0l (long double x)
// Function: _FloatN j0fN (_FloatN x)
// Function: _FloatNx j0fNx (_FloatNx x)

// Function: double j1 (double x)
// Function: float j1f (float x) ¶
// Function: long double j1l (long double x)
// Function: _FloatN j1fN (_FloatN x)
// Function: _FloatNx j1fNx (_FloatNx x)

// I_v(z) : Modified Bessel function of the first kind

// returns the Kaiser window beta factor give the filter's target
// stop-band attenuation (as) [Vaidyanathan:1993]
//  _as     :   target filter's stop-band attenuation [dB], _as > 0

float sincf(float _x) {
    // _x ~ 0 approximation
    //if (fabsf(_x) < 0.01f)
    //    return expf(-lngammaf(1+_x) - lngammaf(1-_x));

    // _x ~ 0 approximation
    // from : http://mathworld.wolfram.com/SincFunction.html
    // sinc(z) = \prod_{k=1}^{\infty} { cos(\pi z / 2^k) }
    if (fabsf(_x) < 0.01f)
        return cosf(M_PI*_x/2.0f)*cosf(M_PI*_x/4.0f)*cosf(M_PI*_x/8.0f);

    return sinf(M_PI*_x)/(M_PI*_x);
}


float kaiser_beta_As(float _as)
{
    _as = fabsf(_as);
    float beta;
    if (_as > 50.0f)
        beta = 0.1102f*(_as - 8.7f);
    else if (_as > 21.0f)
        beta = 0.5842*powf(_as - 21, 0.4f) + 0.07886f*(_as - 21);
    else
        beta = 0.0f;

    return beta;
}


// estimate required filter length given transition bandwidth and
// stop-band attenuation (algorithm from [Vaidyanathan:1993])
//  _df     :   transition bandwidth (0 < _df < 0.5)
//  _as     :   stop-band attenuation [dB] (as > 0)
float estimate_req_filter_len_Kaiser(float _df,
                                     float _as)
{
    if (_df > 0.5f || _df <= 0.0f) {
        printf("index wrong\n");
        return 0.0f;
    } else if (_as <= 0.0f) {
        printf("index wrong\n");
        return 0.0f;
    }
    // compute filter length estimate
    return (_as - 7.95f)/(14.26f*_df);
}



float kaiser_window(unsigned int index,
                  unsigned int window_len,
                  float        beta)
{
    if (index >= window_len) {
        printf("index must be greater than index\n");
        return -1.0f;
    }
    if (beta < 0) {
        printf("beta must be positve\n");
        return -1.0f;
    }
    float t = (float)index - (float)(window_len - 1) / 2;
    float r = 2.0f * t / (float)(window_len);
    float a = j0f(beta * sqrtf(1 - r * r));
    float b = j0f(beta);

    return a / b;
}

// kaiser_filter(120, 0.05087, 80, )

// Design FIR using kaiser window
//  _n      : filter length, _n > 0
//  _fc     : cutoff frequency, 0 < _fc < 0.5
//  _as     : stop-band attenuation [dB], _as > 0
//  _mu     : fractional sample offset, -0.5 < _mu < 0.5
//  _h      : output coefficient buffer, [size: _n x 1]
int kaiser_filter(unsigned int n,
                  float fc,
                  float as, 
                  float mu, 
                  float *h)
{
    if (mu < -0.5f || mu > 0.5f)
        printf("kaiser_filter: mu error\n");
        return -1.0f;
    if (fc <= 0.0f || fc > 0.5f)
        printf("kaiser_filter: fc error\n");
        return -1.0f;
    if (n == 0)
        printf("kaiser_filter: n == 0\n");
        return -1.0f;

    float beta = kaiser_beta_As(as);
    float t, h1, h2;
    
    unsigned int i;
    for (i=0; i<n; i++) {
        t = (float)i - (float)(n-1)/2 + mu;
     
        // sinc prototype
        h1 = sincf(2.0f*fc*t);

        // kaiser window
        h2 = kaiser_window(i,n,beta);

        //printf("t = %f, h1 = %f, h2 = %f\n", t, h1, h2);

        // composite
        h[i] = h1*h2;
    }
}

int main() {
    printf("%f\n", j0(10));
    printf("%f\n", j1(10));
    printf("%f\n",cosf(M_PI*10/2.0f));
    printf("%f\n",cosf(M_PI*110/2.0f));
    printf("%f\n",cosf(M_PI*1200/2.0f));
    printf("%f\n",cosf(M_PI*13000/2.0f));
    printf("%f\n",M_PI);
    // printf("%d\n", kaiser_filter(0, 100, 0.1));

}



#endif