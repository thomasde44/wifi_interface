import matplotlib.pyplot as plt

def read_points_from_file(file_path):
    x_coords = []
    y_coords = []
    with open(file_path, 'r') as file:
        for line in file:
            x, y = map(int, line.strip().split(','))
            x_coords.append(x)
            y_coords.append(y)
    return x_coords, y_coords

def plot_cartesian_points(x_coords, y_coords):
    plt.scatter(x_coords, y_coords, color='blue', marker='o', label='Points')
    plt.xlabel('X-axis')
    plt.ylabel('Y-axis')
    plt.title('Cartesian Points')
    plt.grid(True)
    plt.legend()
    plt.show()

# Replace 'points.txt' with the actual path to your file.
file_path = 'res2.txt'
x_coordinates, y_coordinates = read_points_from_file(file_path)

plot_cartesian_points(x_coordinates, y_coordinates)
