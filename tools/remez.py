import numpy as np
from scipy.signal import firwin, freqz
import matplotlib.pyplot as plt

# Define filter specifications
stop_band_frequency = 44.1e3  # Hz
transition_band = 1e6  # Hz
sampling_rate = 2084000.0  # samples per second

# Calculate normalized frequencies
normalized_stop_band = stop_band_frequency / (0.5 * sampling_rate)
normalized_transition_band = transition_band / (0.5 * sampling_rate)

# Design a lowpass FIR filter using the Hamming window
filter_order = 101  # Adjust as needed
taps = firwin(filter_order + 1, cutoff=normalized_transition_band, window='hamming')

print(taps)
fft_taps = np.fft.fft(taps)

print()
print(fft_taps)

w, h = freqz(taps, worN=8000)
plt.figure(figsize=(10, 6))
plt.plot(0.5 * sampling_rate * w / np.pi, np.abs(h), 'b', label='Time Domain')
plt.plot(0.5 * sampling_rate * np.fft.fftfreq(len(fft_taps), d=1.0/sampling_rate), np.abs(fft_taps), 'r--', label='Frequency Domain')
plt.title('Frequency Response of Hamming Window Lowpass Filter')
plt.xlabel('Frequency [Hz]')
plt.ylabel('Gain')
plt.legend()
plt.grid()
plt.savefig('remez.png')