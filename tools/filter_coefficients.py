import numpy as np
from scipy import signal

# Define filter specifications
cutoff_frequency = 0.1  # Adjust as needed
transition_width = 0.05  # Adjust as needed
filter_order = 50  # Adjust as needed

# Design a Butterworth low-pass filter
b, a = signal.butter(filter_order, cutoff_frequency, btype='low', analog=False, output='ba')

# Extract filter coefficients
filter_coefficients = np.array(a, dtype=np.complex64)

print('filter coeff: \n')
print(filter_coefficients)
print()
# Define complex integer data array (replace with your data)
data_array = np.array([1 + 2j, 2 - 3j, 3 + 4j, 4 - 5j], dtype=np.complex64)

# Convolve data array with the filter
filtered_data = np.convolve(data_array, filter_coefficients, mode='same')

# Print the filtered data
print("Filtered Data:", filtered_data)
