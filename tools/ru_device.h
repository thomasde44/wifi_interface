#ifndef RU_DEVICE_H
#define RU_DEVICE_H

#include <iio.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <signal.h>
#include <stdio.h>
#include <errno.h>
#include <ad9361.h>
#include <pthread.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <sys/resource.h>
#include <complex.h>
#include <fftw3.h>
#include <math.h>
#include <volk/volk.h>
#include <immintrin.h>
#include <alsa/asoundlib.h>
#define MHZ(x) ((long long)(x*1000000.0 + .5))
#define GHZ(x) ((long long)(x*1000000000.0 + .5))

enum iodev { RX, TX };
#define N 16384 / 4  // Length of input sequence
#define M 102  // Length of impulse response
#define process_size (N + M - 1)
# define M_PI		3.14159265358979323846	/* pi */
# define M_PI_2		1.57079632679489661923	/* pi/2 */
FILE *file;
static struct iio_context *ctx = NULL;
static struct iio_device *dev = NULL;
static struct iio_device *phy = NULL;
static struct iio_buffer  *rxbuf = NULL;
static struct iio_channel *rx0_i = NULL;
static struct iio_channel *rx0_q = NULL;

// current libiio buffer and size atm
unsigned int buffer_size;
// struct iio_buffer *buf;
unsigned long items_in_buffer;
// for reading in thread
volatile bool please_refill_buffer, thread_stopped;
// offset when reading rx buffer
off_t byte_offset;

// decimation factor for filter
unsigned int decimation = 0;

// "voltage2","voltage3" are for tx only doing rx 
// pluto has 2 channels...
const char *pluto_channels[] = {"voltage0",
							   "voltage1"};

// POSIX message queue
const char * rx_queue_name 	 = "/rx_queue";
const char * cuda_queue_name = "/cuda_queue";
const char * audio_queue = "/audio_queue";

pthread_mutex_t iio_mutex      = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  iio_condition  = PTHREAD_COND_INITIALIZER;
pthread_cond_t  iio_condition2 = PTHREAD_COND_INITIALIZER;
/* static scratch mem for strings */
static char tmpstr[64];

#define IIO_ENSURE(expr) { \
	if (!(expr)) { \
		(void) fprintf(stderr, "assertion failed (%s:%d)\n", __FILE__, __LINE__); \
		(void) abort(); \
	} \
}

#define CHECK(expr)                                                            \
  if (expr < 0) {                                                              \
    return expr;                                                               \
}


// --------------------------------------------------------------
/* cleanup and exit the pluto */
// --------------------------------------------------------------
static void shutdown()
{
	printf("-Destroying buffers-\n");
	if (rxbuf) { iio_buffer_destroy(rxbuf); }

	printf("-Disabling streaming channels-\n");
	if (rx0_i) { iio_channel_disable(rx0_i); }
	if (rx0_q) { iio_channel_disable(rx0_q); }

	printf("-Destroying context-\n");
	if (ctx) { iio_context_destroy(ctx); }
	exit(0);
}


// --------------------------------------------------------------
// enable_rx_channel() - find the channel for the ad9361 and if 
//   					 it exists enable it
// 
// struct iio_channel **chn  - libiio channel object
// const char *pluto_channel - string name of channel (internally defined)
// --------------------------------------------------------------
static bool enable_rx_channel(struct iio_channel **chn, const char *pluto_channel)
{
	*chn =
	    iio_device_find_channel(dev, pluto_channel, false);
	
	if (!*chn) 
	{
		return false;
	}
	iio_channel_enable(*chn);
	return true;
}

// --------------------------------------------------------------
// process_param() - check to see if the config param exists then set it
// 
// struct iio_channel *chn  - libiio channel object
// const char *attr			- string of attribute to set
// const char *string		- convert all value to string numeric or not 
// 							  and use a generic write string label set attr
// --------------------------------------------------------------
static bool process_param(struct iio_channel *chn,
						  const char *attr,
						  const char *string)
{
	unsigned int ret = 0;
	if (chn)
	{
	    ret = iio_channel_attr_write(chn,
			    attr, string);
	}
	else if (iio_device_find_attr(phy, attr))
	{
	    ret = iio_device_attr_write(phy, attr, string);
	}
	else
	{
	    ret = iio_device_debug_attr_write(phy,
			    attr, string);
	}
	if (ret < 0) {
	    printf("Unable to write attribute\n");
		return false;
	}
	return true;
}

/* check return value of attr_write function */
static void errchk(int v, const char* what) {
	 if (v < 0) { fprintf(stderr, "Error %d writing to channel \"%s\"\nvalue may not be supported.\n", v, what); shutdown(); }
}

// --------------------------------------------------------------
// get_uri()  - after you have found a usb associated resource identifier
// 			    get the context
// char** uri - resource identifier string
// --------------------------------------------------------------
int get_uri(char** uri)
{
    struct iio_scan_context *ctx = iio_create_scan_context("usb", 0);
    
    if (!ctx){
		fprintf(stderr, "Unable to create scan context: %s\n", strerror(errno));
        return -1;
    }

	struct iio_context_info **info;
	int ret = iio_scan_context_get_info_list(ctx, &info);

	if (ret < 0) {
		iio_scan_context_destroy(ctx);
		fprintf(stderr, "Unable to scan for Pluto devices: %s\n", strerror(errno));
        return -1;
	}

	if (ret == 0) {
		iio_context_info_list_free(info);
		iio_scan_context_destroy(ctx);
		fprintf(stderr, "No Pluto device found\n");
	}

	if (ret > 1) {
		printf("More than one Pluto found:\n");

		for (unsigned int i = 0; i < (size_t) ret; i++) {
			printf("\t%d: %s [%s]\n", i,
				iio_context_info_get_description(info[i]),
				iio_context_info_get_uri(info[i]));
		}

		printf("We will use the first one.\n");
	}

	* uri = strdup(iio_context_info_get_uri(info[0]));
	iio_context_info_list_free(info);
	iio_scan_context_destroy(ctx);
    if (*uri == NULL) {
        printf("Memory allocation error for URI: %s\n", strerror(errno));
        return -1;
    }
	return 0;
}

/* finds AD9361 streaming IIO devices */


// --------------------------------------------------------------
// get_ad9361_stream_dev() - function to get transceiver streaming dev object
// enum iodev d 		   - user defined label for either TX or RX (convenience)
// struct iio_device **dev - device obhect to instatiate
// --------------------------------------------------------------
static bool get_ad9361_stream_dev(enum iodev d, struct iio_device **dev)
{
	switch (d) {
	case TX: *dev = iio_context_find_device(ctx, "cf-ad9361-dds-core-lpc"); return *dev != NULL;
	case RX: *dev = iio_context_find_device(ctx, "cf-ad9361-lpc");  return *dev != NULL;
	default: IIO_ENSURE(0); return false;
	}
}

// --------------------------------------------------------------
/* helper function generating channel names */
// get_ch_name() - string label type of channel
// id			 - id for the channel 0 or 1
// --------------------------------------------------------------
static char* get_ch_name(const char* type, int id)
{
    memset(tmpstr, 0, sizeof(tmpstr));
	snprintf(tmpstr, sizeof(tmpstr), "%s%d", type, id);
	return tmpstr;
}


// --------------------------------------------------------------
/* finds AD9361 streaming IIO channels */
// get_ad9361_stream_ch() 	- get stream channel
// enum iodev d 			- convenience label either rx or tx defined by user
// struct iio_device *dev 	- libiio dev object to get
// int chid 				- channel id ad9361 differentiates tx and rx with channel id's
// struct iio_channel **chn - channel to get
// --------------------------------------------------------------
static bool get_ad9361_stream_ch(enum iodev d, struct iio_device *dev, int chid, struct iio_channel **chn)
{
	*chn = iio_device_find_channel(dev, get_ch_name("voltage", chid), d == TX);
	if (!*chn)
		*chn = iio_device_find_channel(dev, get_ch_name("altvoltage", chid), d == TX);
	return *chn != NULL;
}

/* returns ad9361 phy device */
static struct iio_device* get_ad9361_phy(void)
{
	struct iio_device *dev =  iio_context_find_device(ctx, "ad9361-phy");
	IIO_ENSURE(dev && "No ad9361-phy found");
	return dev;
}

/* finds AD9361 phy IIO configuration channel with id chid */
static bool get_phy_chan(enum iodev d, int chid, struct iio_channel **chn, char * name, bool enable)
{
	switch (d) {
	case RX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name(name, chid), enable); return *chn != NULL;
	case TX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name(name, chid), enable);  return *chn != NULL;
	default: IIO_ENSURE(0); return false;
	}
}

/* finds AD9361 local oscillator IIO configuration channels */
static bool get_lo_chan(enum iodev d, struct iio_channel **chn)
{
	switch (d) {
	 // LO chan is always output, i.e. true
	case RX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name("altvoltage", 0), true); return *chn != NULL;
	case TX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name("altvoltage", 1), true); return *chn != NULL;
	default: IIO_ENSURE(0); return false;
	}
}

static void wr_ch_str(struct iio_channel *chn, const char* what, const char* str)
{
	errchk(iio_channel_attr_write(chn, what, str), what);
}


static void wr_ch_double(struct iio_channel *chn, const char* what, double val)
{
	errchk(iio_channel_attr_write_double(chn, what, val), what);
}

/* write attribute: long long int */
static void wr_ch_lli(struct iio_channel *chn, const char* what, long long val)
{
	errchk(iio_channel_attr_write_longlong(chn, what, val), what);
}

// --------------------------------------------------------------
// create_message_queue() - create queue to buffer in read IQ data
// 
// const char* name - name of the queue
// struct mq_attr* attr - specific attributes of the message queue
// --------------------------------------------------------------
mqd_t create_message_queue(const char* name, struct mq_attr* attr)
{

	// printf("create queue size: %lu\n", attr->mq_msgsize);
    mqd_t mq = mq_open(name, O_CREAT | O_RDWR, 0770, attr);
    if (mq == (mqd_t)-1) {
        perror("mq_open error");
        exit(1);
    } else {
        printf("mq_open successful %s\n", name);
    }
	// struct mq_attr attr2;
    // if (mq_getattr(mq, &attr2) == -1) {
    //     perror("mq_getattr error");
    //     return false; // Return false for failure
    // }
	// printf("create queue size after: %lu\n", attr2.mq_msgsize);
    return mq;
}

// --------------------------------------------------------------
// destroy_message_queue() - remove queue from system
// const char* name - name same as the one used to create it
// mqd_t mq - message queue instatiated
// --------------------------------------------------------------
bool destroy_message_queue(const char* name, mqd_t mq)
{
    if (mq_close(mq) == -1) {
        perror("mq_close error");
        return false;
    }

    if (mq_unlink(name) == -1) {
        perror("mq_unlink error");
        return false;
    }

	return true;
}

// --------------------------------------------------------------
// check_message_queue_existence - messages queues are linux system
// 					objects that persist beyond the program. The
// 					program should unlink the queue but incase we
// 					can check and see if it already exists and delete
// 
// const char* name 			 - name of queue to search for	 
// --------------------------------------------------------------
bool check_message_queue_existence(const char* name)
{
    mqd_t mq;
    int flags = O_RDWR;

    mq = mq_open(name, flags);
    if (mq == (mqd_t)-1) {
        if (errno == ENOENT) {
            printf("queue does not exist\n");
			return true;
        } else {
            perror("mq_open error");
            exit(EXIT_FAILURE);
        }
    } else {
        printf("queue exists.\n");
		printf("deleted queue: %d\n", destroy_message_queue(name, mq));
		return false;
    }
}

// --------------------------------------------------------------
// get_message_count - input a message queue object and get how 
// 					   many message are currently in the queue
// --------------------------------------------------------------
int get_message_count(mqd_t mq)
{
    struct mq_attr attr;
    if (mq_getattr(mq, &attr) == -1) {
        perror("mq_getattr error");
        exit(EXIT_FAILURE);
    }
    return attr.mq_curmsgs;
}


// --------------------------------------------------------------
// size_t size - size of buffer to allocate
// size_t alignment - size of type alignment ex 16 for 16 bit alignment
// --------------------------------------------------------------
void* allocate_aligned_buffer(size_t size, size_t alignment)
{
    void* ptr = malloc(size + alignment - 1);
    if (ptr) {
        uintptr_t misalignment = (uintptr_t)ptr % alignment;
        if (misalignment != 0) {
            // Adjust the pointer to the next aligned address
            ptr = (void*)((uintptr_t)ptr + (alignment - misalignment));
        }
    }
    return ptr;
}


struct rx_message 
{
	size_t size;
	void * data;
};

struct thrd_arg_msg_q
{
    mqd_t mq1;
	mqd_t mq2;
};

// --------------------------------------------------------------
// const int16_t* message - int 16 message buffer to send
// size_t message_size - size of the message being sent
// mqd_t mq - message queue to enqueue data
// --------------------------------------------------------------
// bool send_message(const char* message, size_t message_size, mqd_t mq) {
//     // Enqueue data
//     printf("in send\n");
// 	if (mq_send(mq, message, message_size, 0) == -1) {
//         perror("mq_send error");
//         return true; // Return true for failure
//     }
//     return false; // Return false for success
// }

bool send_message(struct rx_message *message, mqd_t mq)
{
    // Enqueue data

    // printf("in send, msg sz: %u \n", (unsigned int)message->size);
    if (mq_send(mq, (const char*)message->data, message->size, 0) == -1) {
        printf("mq_send error\n");
        return false; // Return true for failure
    }
    return true; // Return false for success
}

double complex normalizec(double complex z)
{
    // Compute the magnitude of the complex number
    double magnitude = cabs(z);
    
    // Check for division by zero (if magnitude is zero)
    if (magnitude == 0) {
        // printf("Error: Division by zero value is 0+0i.\n");
        return 0 + 0 * I;
    }
    
    // Normalize the complex number
    double real_part = creal(z) / magnitude;
    double imag_part = cimag(z) / magnitude;
    
    return real_part + imag_part * I;
}

void fix_complex_array(complex double arr[], int size)
{
    for (int i = 0; i < size; i++) {
        // Check for NaN using isnan() function from math.h
        if (isnan(creal(arr[i])) || isnan(cimag(arr[i]))) {
            arr[i] = 0 + 0 * I; // Setting to 0 + 0i
        }
        
        // Check for overflow by checking if the absolute value is too large
        if (fabs(creal(arr[i])) > 1e50 || fabs(cimag(arr[i])) > 1e50) {
            arr[i] = 0 + 0 * I; // Setting to 0 + 0i
        }
    }
}

// ==============================================================================
// pre computed taps for lowpass filter related to the sampling rate and stop band
// stopband: 44.1kHz
// sample rate: 2084000
// ==============================================================================
double complex * lowpass_fft(double complex * x)
{
    // #define N 8092  // Length of input sequence
    // #define M 102  // Length of impulse response
    double complex h[] = {
        1.00000000e+00 + 0.00000000e+00*I, -9.98541183e-01 - 3.07647253e-02*I,
        9.98104259e-01 + 6.15609635e-02*I, -9.94751513e-01 - 9.21773023e-02*I,
        9.92424228e-01 + 1.22888751e-01*I, -9.87186550e-01 - 1.53239698e-01*I,
        9.82981452e-01 + 1.83751079e-01*I, -9.75874990e-01 - 2.13719921e-01*I,
        9.69811750e-01 + 2.43917446e-01*I, -9.60859742e-01 - 2.73388168e-01*I,
        9.52965078e-01 + 3.03160016e-01*I, -9.42197767e-01 - 3.32017683e-01*I,
        9.32505341e-01 + 3.61254494e-01*I, -9.19959856e-01 - 3.89385609e-01*I,
        9.08510147e-01 + 4.17980991e-01*I, -8.94230369e-01 - 4.45273816e-01*I,
        8.81070515e-01 + 4.73124875e-01*I, -8.65106909e-01 - 4.99469707e-01*I,
        8.50290530e-01 + 5.26477607e-01*I, -8.32699956e-01 - 5.51767002e-01*I,
        8.16286944e-01 + 5.77837564e-01*I, -7.97132446e-01 - 6.01966481e-01*I,
        7.79188740e-01 + 6.27010846e-01*I, -7.58539309e-01 - 6.49876693e-01*I,
        7.39136631e-01 + 6.73812070e-01*I, -7.17066953e-01 - 6.95314608e-01*I,
        6.96282534e-01 + 7.18065160e-01*I, -6.72872716e-01 - 7.38106209e-01*I,
        6.50788987e-01 + 7.59604143e-01*I, -6.26124272e-01 - 7.78086990e-01*I,
        6.02828524e-01 + 7.98273976e-01*I, -5.76999000e-01 - 8.15102340e-01*I,
        5.52583015e-01 + 8.33931443e-01*I, -5.25683331e-01 - 8.49007731e-01*I,
        5.00242948e-01 + 8.66446201e-01*I, -4.72372074e-01 - 8.79668623e-01*I,
        4.46006655e-01 + 8.95702108e-01*I, -4.17267762e-01 - 9.06959895e-01*I,
        3.90079423e-01 + 9.21599056e-01*I, -3.60580113e-01 - 9.30764564e-01*I,
        3.32672300e-01 + 9.44055436e-01*I, -3.02526010e-01 - 9.50972120e-01*I,
        2.73999772e-01 + 9.63009309e-01*I, -2.43331947e-01 - 9.67483815e-01*I,
        2.14271188e-01 + 9.78392151e-01*I, -1.83254236e-01 - 9.80323578e-01*I,
        1.53619989e-01 + 9.89636422e-01*I, -1.22909689e-01 - 9.92593319e-01*I,
        8.36998444e-02 + 9.03265172e-01*I, -2.88555018e-02 - 4.67841918e-01*I,
        2.11788767e-03 + 6.87410024e-02*I,  2.77555756e-17 - 5.55111512e-17*I,
        2.11788767e-03 - 6.87410024e-02*I, -2.88555018e-02 + 4.67841918e-01*I,
        8.36998444e-02 - 9.03265172e-01*I, -1.22909689e-01 + 9.92593319e-01*I,
        1.53619989e-01 - 9.89636422e-01*I, -1.83254236e-01 + 9.80323578e-01*I,
        2.14271188e-01 - 9.78392151e-01*I, -2.43331947e-01 + 9.67483815e-01*I,
        2.73999772e-01 - 9.63009309e-01*I, -3.02526010e-01 + 9.50972120e-01*I,
        3.32672300e-01 - 9.44055436e-01*I, -3.60580113e-01 + 9.30764564e-01*I,
        3.90079423e-01 - 9.21599056e-01*I, -4.17267762e-01 + 9.06959895e-01*I,
        4.46006655e-01 - 8.95702108e-01*I, -4.72372074e-01 + 8.79668623e-01*I,
        5.00242948e-01 - 8.66446201e-01*I, -5.25683331e-01 + 8.49007731e-01*I,
        5.52583015e-01 - 8.33931443e-01*I, -5.76999000e-01 + 8.15102340e-01*I,
        6.02828524e-01 - 7.98273976e-01*I, -6.26124272e-01 + 7.78086990e-01*I,
        6.50788987e-01 - 7.59604143e-01*I, -6.72872716e-01 + 7.38106209e-01*I,
        6.96282534e-01 - 7.18065160e-01*I, -7.17066953e-01 + 6.95314608e-01*I,
        7.39136631e-01 - 6.73812070e-01*I, -7.58539309e-01 + 6.49876693e-01*I,
        7.79188740e-01 - 6.27010846e-01*I, -7.97132446e-01 + 6.01966481e-01*I,
        8.16286944e-01 - 5.77837564e-01*I, -8.32699956e-01 + 5.51767002e-01*I,
        8.50290530e-01 - 5.26477607e-01*I, -8.65106909e-01 + 4.99469707e-01*I,
        8.81070515e-01 - 4.73124875e-01*I, -8.94230369e-01 + 4.45273816e-01*I,
        9.08510147e-01 - 4.17980991e-01*I, -9.19959856e-01 + 3.89385609e-01*I,
        9.32505341e-01 - 3.61254494e-01*I, -9.42197767e-01 + 3.32017683e-01*I,
        9.52965078e-01 - 3.03160016e-01*I, -9.60859742e-01 + 2.73388168e-01*I,
        9.69811750e-01 - 2.43917446e-01*I, -9.75874990e-01 + 2.13719921e-01*I,
        9.82981452e-01 - 1.83751079e-01*I, -9.87186550e-01 + 1.53239698e-01*I,
        9.92424228e-01 - 1.22888751e-01*I, -9.94751513e-01 + 9.21773023e-02*I,
        9.98104259e-01 - 6.15609635e-02*I, -9.98541183e-01 + 3.07647253e-02*I
    };
    for (int i = 0; i < N; i++) {
        x[i] = normalizec(x[i]);
    }
    printf("hi\n");
    fix_complex_array(x, (N + M - 1));
    printf("hi2\n");
    // for (int i = 0; i < N + M - 1; i++)
    // {
    //     printf("x: %.8f + %.8fi\n", creal(x[i]), cimag(x[i]));
    // }

	double complex * y = (double complex *)malloc((N + M - 1) * sizeof(double complex));
    // Create FFTW plans

    fftw_complex* x_freq = fftw_malloc(sizeof(fftw_complex) * (N + M - 1));

    fftw_complex* h_freq = fftw_malloc(sizeof(fftw_complex) * (N + M - 1));

    fftw_complex* y_freq = fftw_malloc(sizeof(fftw_complex) * (N + M - 1));

    fftw_plan x_plan = fftw_plan_dft_1d(N + M - 1, x, x_freq, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan h_plan = fftw_plan_dft_1d(N + M - 1, h, h_freq, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan y_plan = fftw_plan_dft_1d(N + M - 1, y_freq, y_freq, FFTW_BACKWARD, FFTW_ESTIMATE);
	int count = 0;

    fftw_execute(x_plan);
    for (int i = 0; i < (N + M - 1); i++)
    {
        x_freq[i] = normalizec(x_freq[i]);
        // printf("x2: %.8f + %.8fi\n", creal(x[i]), cimag(x[i]));
        // printf("x_freq: %.8f + %.8fi\n", creal(x_freq[i]), cimag(x_freq[i]));
    }
    // for (int i = 0; i < M; i++) {
    // 	h_freq[i] = h[i];
    // }
    fftw_execute(h_plan);

    // Perform element-wise multiplication in the frequency domain
    for (int i = 0; i < N + M - 1; i++) {
        y_freq[i] = x_freq[i] * h_freq[i];
    }

    // Perform inverse FFT to obtain the convolution result
    fftw_execute(y_plan);

    // Normalize the result by scaling
    // for (int i = 0; i < N + M - 1; i++) {
    //     y[i] = y_freq[i]; // / (N + M - 1);
    // }

    // Print the convolution result
    // printf("\n\nConvolution result:\n");
    for (int i = 0; i < N + M - 1; i++) {
        y_freq[i] = normalizec(y_freq[i]);
    }
    // exit(1);

    // Destroy FFTW plans and free allocated memory
    fftw_destroy_plan(x_plan);
    fftw_destroy_plan(h_plan);
    fftw_destroy_plan(y_plan);
    fftw_free(x_freq);
    fftw_free(h_freq);
    // fftw_free(y_freq);
    free(y);
    return y_freq;
}

// returns normalized value of the vector after squelch operation
double complex * squelch(double threshold_db, double alpha, double complex * x)
{

    threshold_db = pow(10.0, threshold_db / 10);
    threshold_db = 10 * log10(threshold_db);
    double one_minus_alpha = (1.0 - alpha);

    double complex prev = 0;
    double complex current = 0;
    double x_result = 0;
    for (int i = 0; i < process_size; i++)
    {
        // printf("%d\n", i);
        current = x[i];
        double magnitude_2 = cabs(current) * cabs(current);
        // maybe can skip if alpha == 1
        x_result = (alpha * magnitude_2) + (one_minus_alpha * prev);

        if (x_result < threshold_db)
        {
            x[i] = 0.0 + 0.0*I;
        }
        else
        {
            x[i] = normalizec(x[i]);
        }

        x_result = 0;
        prev = current;

    }
    return x;
}

void split_complex_array(float complex * complex_array, float * real_array, float * imag_array, size_t length) {
    for (size_t i = 0; i < length; i++) {
        real_array[i] = creal(complex_array[i]);   // Extract real part
        imag_array[i] = cimag(complex_array[i]);   // Extract imaginary part
    }
}

float  complex * convert_complex_dbl_to_flt(double complex * x, bool conjuagte_flag)
{

    // for (int i = 0; i < process_size; i++)
    // {
    //     printf("x: %.8f + %.8fi\n", creal(x[i]), cimag(x[i]));
    // }

    float complex * x_float_res = (float complex *)malloc(process_size * sizeof(float complex));

    if (x_float_res == NULL) {
        printf("Memory allocation failed\n");
        exit(1);
    }

    for (int i = 0; i < process_size; i++)
    {
        x_float_res[i] = (float complex)x[i];
 
    }
    if (conjuagte_flag)
    {
        volk_32fc_conjugate_32fc(x_float_res, x_float_res, process_size);
    }

    return x_float_res;
}


float * quadrature_demod(double complex * x)
{
    float complex * x_float = (float complex *)malloc(process_size * sizeof(float complex));
    float complex * x_float_conj = (float complex *)malloc(process_size * sizeof(float complex));
    float complex * x_float_res = (float complex *)malloc(process_size * sizeof(float complex *));

    float * x_float_demod_res = (float *)malloc(process_size * sizeof(float *));

    // if (x_float_res == NULL || x_float_demod_res == NULL ||
    //     x_float == NULL || x_float_conj == NULL) {
    //     // Handle memory allocation failure
    //     perror("Memory allocation failed");
    //     exit(EXIT_FAILURE);
    // }

    // exit(1);
    // Initialize the memory block with zeros
    memset(x_float_res, 0, process_size * sizeof(float complex));
    memset(x_float_demod_res, 0, process_size * sizeof(float complex));
    x_float = convert_complex_dbl_to_flt(x, false);
    x_float_conj = convert_complex_dbl_to_flt(x, true);

    // for (int i = 0; i < process_size; i++)
    // {
    //     printf("x: %.8f + %.8fi\n", creal(x[i]), cimag(x[i]));
    // }

    // // exit(1);

    volk_32fc_x2_multiply_conjugate_32fc_u(x_float_res, x_float, x_float_conj, process_size);

    // // for (int i = 0; i < process_size; i++)
    // // {
    // //     printf("conj res: %.8f + %.8fi\n", creal(x_float_res[i]), cimag(x_float_res[i]));
    // // }
    volk_32fc_s32f_atan2_32f(x_float_demod_res, x_float_res, 1, process_size);

    free(x_float);
    free(x_float_conj);
    free(x_float_res);

    return x_float_demod_res;

}




// --------------------------------------------------------------
// mqd_t mq 	  		 - message queue object
// char **message 		 - message buffer
// unsigned int priority - priority for insertion
// --------------------------------------------------------------
// bool receive_message(mqd_t mq, char **message, size_t *message_size, unsigned int priority) {
//     // Get the size of the incoming message
//     struct mq_attr attr;
//     if (mq_getattr(mq, &attr) == -1) {
//         perror("mq_getattr error");
//         return false; // Return false for failure
//     }

//     // Allocate memory for the received message
//     *message_size = attr.mq_msgsize;
//     *message = (char *)malloc(*message_size);
//     if (*message == NULL) {
//         perror("Memory allocation error");
//         return false; // Return false for failure
//     }

//     // Receive the message
//     ssize_t bytes_received = mq_receive(mq, *message, *message_size, &priority);
//     if (bytes_received == -1) {
//         perror("mq_receive error");
//         free(*message); // Free the allocated memory
//         return false; // Return false for failure
//     }

//     return true; // Return true for success
// }

bool receive_message(mqd_t mq, struct rx_message* message, unsigned int priority)
{
    // Get the size of the incoming message
	// printf("in recv ");
    struct mq_attr attr;
    if (mq_getattr(mq, &attr) == -1) {
        perror("mq_getattr error");
        return false; // Return false for failure
    }

    // Allocate memory for the received message
    message->size = attr.mq_msgsize;
    message->data = malloc(message->size);
    if (message->data == NULL) {
        printf("Memory allocation error");
        return false; // Return false for failure
    }

    // Receive the message
    size_t bytes_received = mq_receive(mq, message->data, message->size, &priority);

    if (bytes_received == -1) {
        printf("mq_receive error");
        free(message->data); // Free the allocated memory
        return false; // Return false for failure
    }

    return true; // Return true for success
}

// ---------------------------------------------------------------
// create_rx_message size of msg to send
// ---------------------------------------------------------------
struct rx_message* create_rx_message(size_t size)
{

    struct rx_message* message = (struct rx_message*)malloc(sizeof(struct rx_message));
    if (message == NULL) {
        perror("malloc error");
        return NULL; // Return NULL to indicate failure
    }

    // Allocate memory for the data buffer
	
    message->data = malloc(size);
    if (message->data == NULL) {
        free(message); // Clean up the allocated struct if data allocation fails
        perror("malloc error");
        return NULL; // Return NULL to indicate failure
    }

    message->size = size;
    return message;
}


// Signal handler function
void signal_handler(int signum) {
    if (file) {
        fclose(file);  // Close the file if it's open
        file = NULL;   // Reset file pointer
    }
    exit(signum);  // Exit the program with the received signal number
}

void convert_to_audio(const double frequency, double phase, unsigned int sample_rate, unsigned int buffer_size, float * process_data)
{
    signal(SIGINT, signal_handler);
    printf("in convert_to_audio\n");
    // FILE *file;
    short buffer[buffer_size];  // Buffer to hold audio samples
    // double phase = 0.0;         // Phase accumulator for sine wave
    // const double frequency = 440.0;  // Frequency of the sine wave (A4 note in Hz)
    // const double amplitude = 0.5 * 32767;  // Amplitude for 16-bit PCM

    // Open WAV file for writing
    file = fopen("output.wav", "ab");
    if (!file) {
        printf("Error opening output file\n");
        exit(EXIT_FAILURE);
    }
    printf("in convert_to_audio2\n");
    // Calculate data size (number of samples * number of channels * bytes per sample)
    const int dataSize = buffer_size * 2;  // 2 bytes per sample (16-bit PCM)

    // Write WAV header (44 bytes for a standard 16-bit, mono WAV file)
    {
        unsigned char header[44] = {
            'R', 'I', 'F', 'F',  // Chunk ID
            0, 0, 0, 0,          // Chunk size (to be filled)
            'W', 'A', 'V', 'E',  // Format
            'f', 'm', 't', ' ',  // Subchunk1 ID
            16, 0, 0, 0,         // Subchunk1 size
            1, 0,               // Audio format (1 = PCM)
            1, 0,               // Number of channels (1 for mono)
            sample_rate & 0xFF, (sample_rate >> 8) & 0xFF, (sample_rate >> 16) & 0xFF, (sample_rate >> 24) & 0xFF,  // Sample rate
            (sample_rate * 2) & 0xFF, ((sample_rate * 2) >> 8) & 0xFF, ((sample_rate * 2) >> 16) & 0xFF, ((sample_rate * 2) >> 24) & 0xFF,  // Byte rate
            2, 0,               // Block align (2 bytes per sample)
            16, 0,              // Bits per sample (16-bit PCM)
            'd', 'a', 't', 'a', // Subchunk2 ID
            0, 0, 0, 0          // Subchunk2 size (to be filled)
        };
        fwrite(header, 1, sizeof(header), file);
    }
    printf("in convert_to_audio3\n");
    // Generate sine wave samples in the buffer
    for (int i = 0; i < buffer_size; i++) {
        // printf(" %f \n", (float)process_data[i]);
        buffer[i] = (short)((process_data[i]*32767) * sin(2.0 * M_PI * frequency * phase / sample_rate));
        phase += 1.0;  // Increment phase for the next sample
        if (phase >= sample_rate) {
            phase -= sample_rate;  // Reset phase if it exceeds the sample rate
        }
    }
    printf("in convert_to_audio4\n");
    // Write the buffer to the WAV file
    fwrite(buffer, sizeof(short), buffer_size, file);

    printf("Current file position: %ld\n", ftell(file));

    fflush(file);
    // Update WAV header with data size
    fseek(file, 4, SEEK_SET);
    const int chunkSize = 36 + dataSize;
    fwrite(&chunkSize, 4, 1, file);
    fseek(file, 40, SEEK_SET);
    fwrite(&dataSize, 4, 1, file);

    // Close the WAV file
    fclose(file);

}


#endif // RU_DEVICE_H