#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pylab import *

sample_rate = 1000

cf = 16 # carrier frequency
mf = 4 # modulation frequency

mod_index = .5 # modulation index
fm_int = 0 # FM integral

dt = []
dfm = []

for n in range(int(sample_rate)):
  t = n / sample_rate # time seconds
  mod = cos(2 * pi * mf * t) # modulation
  fm_int += mod * mod_index / sample_rate # modulation integral
  fm = cos(2 * pi * cf * (t + fm_int)) # generate FM signal
  dt.append(t)
  dfm.append(fm)
  
plot(dt,dfm)
ylim(-1.2,1.2)
gcf().set_size_inches(4,3)
savefig('simple_fm_generator.png')
show()