#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFFER_SIZE 4096

int main() {
    int fd;
    ssize_t bytesRead;
    char buffer[BUFFER_SIZE];

    // Open the block device in read-only mode
    fd = open("/dev/sda1", O_RDONLY);
    if (fd == -1) {
        perror("Failed to open the block device");
        return 1;
    }

    while (1) {
        // Read data from the block device
        bytesRead = read(fd, buffer, BUFFER_SIZE);
        if (bytesRead == -1) {
            perror("Failed to read from the block device");
            close(fd);
            return 1;
        }

        // Print the read data
        printf("Data read from /dev/sda1:\n");
        printf("%.*s\n", (int)bytesRead, buffer);

        // Sleep for a while before the next read
        sleep(1);
    }

    // Close the block device (this will not be reached in the while loop)
    close(fd);

    return 0;
}
