#include <iostream>
#include "mat.h"

int main() {
    // Open the .mat file
    MATFile *pmat = matOpen("lowpass_44100.mat", "r");
    if (pmat == NULL) {
        std::cout << "Error opening the .mat file." << std::endl;
        return 1;
    }

    // Read a variable from the .mat file
    mxArray *arr = matGetVariable(pmat, "myVariable");
    if (arr == NULL) {
        std::cout << "Error reading the variable from the .mat file." << std::endl;
        matClose(pmat);
        return 1;
    }

    // Process the data
    double* data = mxGetPr(arr);
    int numElements = mxGetNumberOfElements(arr);
    for (int i = 0; i < numElements; i++) {
        std::cout << "Element " << i << ": " << data[i] << std::endl;
    }

    // Clean up
    mxDestroyArray(arr);
    matClose(pmat);

    return 0;
}
