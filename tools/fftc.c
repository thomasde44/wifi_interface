#include <stdio.h>
#include <complex.h>
#include <fftw3.h>
// #include <iio/fmcomms2_source.h>
#define N 4096  // Length of input sequence
#define M 121  // Length of impulse response

#include <stdbool.h>
#include <stdint.h>
// #include <string.h>
#include <signal.h>
// #include <stdio.h>
#include <iio.h>
#include <time.h>
#include <string.h>
#if defined(__CUDA_CODE__) || defined(__cplusplus)
extern "C" {
#endif

/* helper macros */
#define MHZ(x) ((long long)(x*1000000.0 + .5))
#define GHZ(x) ((long long)(x*1000000000.0 + .5))

#define IIO_ENSURE(expr) { \
	if (!(expr)) { \
		(void) fprintf(stderr, "assertion failed (%s:%d)\n", __FILE__, __LINE__); \
		(void) abort(); \
	} \
}

/* RX is input, TX is output */
enum iodev { RX, TX };

/* common RX and TX streaming params */
struct stream_cfg {
	long long bw_hz; // Analog banwidth in Hz
	long long fs_hz; // Baseband sample rate in Hz
	long long lo_hz; // Local oscillator frequency in Hz
	const char* rfport; // Port name
};

/* static scratch mem for strings */
static char tmpstr[64];

/* IIO structs required for streaming */
static struct iio_context *ctx   = NULL;
static struct iio_channel *rx0_i = NULL;
static struct iio_channel *rx0_q = NULL;
static struct iio_channel *tx0_i = NULL;
static struct iio_channel *tx0_q = NULL;
static struct iio_buffer  *rxbuf = NULL;
static struct iio_buffer  *txbuf = NULL;

static bool stop;

/* cleanup and exit */
static void shutdown()
{
	printf("* Destroying buffers\n");
	if (rxbuf) { iio_buffer_destroy(rxbuf); }
	if (txbuf) { iio_buffer_destroy(txbuf); }

	printf("* Disabling streaming channels\n");
	if (rx0_i) { iio_channel_disable(rx0_i); }
	if (rx0_q) { iio_channel_disable(rx0_q); }
	if (tx0_i) { iio_channel_disable(tx0_i); }
	if (tx0_q) { iio_channel_disable(tx0_q); }

	printf("* Destroying context\n");
	if (ctx) { iio_context_destroy(ctx); }
	// exit(0);
}

static void handle_sig(int sig)
{
	printf("Waiting for process to finish... Got signal %d\n", sig);
	stop = true;
}

/* check return value of attr_write function */
static void errchk(int v, const char* what) {
	 if (v < 0) { fprintf(stderr, "Error %d writing to channel \"%s\"\nvalue may not be supported.\n", v, what); shutdown(); }
}

/* write attribute: long long int */
static void wr_ch_lli(struct iio_channel *chn, const char* what, long long val)
{
	errchk(iio_channel_attr_write_longlong(chn, what, val), what);
}

/* write attribute: string */
static void wr_ch_str(struct iio_channel *chn, const char* what, const char* str)
{
	errchk(iio_channel_attr_write(chn, what, str), what);
}

/* helper function generating channel names */
static char* get_ch_name(const char* type, int id)
{
	snprintf(tmpstr, sizeof(tmpstr), "%s%d", type, id);
	return tmpstr;
}

/* returns ad9361 phy device */
static struct iio_device* get_ad9361_phy(void)
{
	struct iio_device *dev =  iio_context_find_device(ctx, "ad9361-phy");
	IIO_ENSURE(dev && "No ad9361-phy found");
	return dev;
}

/* finds AD9361 streaming IIO devices */
static bool get_ad9361_stream_dev(enum iodev d, struct iio_device **dev)
{
	switch (d) {
	case TX: *dev = iio_context_find_device(ctx, "cf-ad9361-dds-core-lpc"); return *dev != NULL;
	case RX: *dev = iio_context_find_device(ctx, "cf-ad9361-lpc");  return *dev != NULL;
	default: IIO_ENSURE(0); return false;
	}
}

/* finds AD9361 streaming IIO channels */
static bool get_ad9361_stream_ch(enum iodev d, struct iio_device *dev, int chid, struct iio_channel **chn)
{
	*chn = iio_device_find_channel(dev, get_ch_name("voltage", chid), d == TX);
	if (!*chn)
		*chn = iio_device_find_channel(dev, get_ch_name("altvoltage", chid), d == TX);
	return *chn != NULL;
}

/* finds AD9361 phy IIO configuration channel with id chid */
static bool get_phy_chan(enum iodev d, int chid, struct iio_channel **chn)
{
	switch (d) {
	case RX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name("voltage", chid), false); return *chn != NULL;
	case TX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name("voltage", chid), true);  return *chn != NULL;
	default: IIO_ENSURE(0); return false;
	}
}

/* finds AD9361 local oscillator IIO configuration channels */
static bool get_lo_chan(enum iodev d, struct iio_channel **chn)
{
	switch (d) {
	 // LO chan is always output, i.e. true
	case RX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name("altvoltage", 0), true); return *chn != NULL;
	case TX: *chn = iio_device_find_channel(get_ad9361_phy(), get_ch_name("altvoltage", 1), true); return *chn != NULL;
	default: IIO_ENSURE(0); return false;
	}
}

/* applies streaming configuration through IIO */
bool cfg_ad9361_streaming_ch(struct stream_cfg *cfg, enum iodev type, int chid)
{
	struct iio_channel *chn = NULL;

	// Configure phy and lo channels
	printf("* Acquiring AD9361 phy channel %d\n", chid);
	if (!get_phy_chan(type, chid, &chn)) {	return false; }
	wr_ch_str(chn, "rf_port_select",     cfg->rfport);
	wr_ch_lli(chn, "rf_bandwidth",       cfg->bw_hz);
	wr_ch_lli(chn, "sampling_frequency", cfg->fs_hz);

	// Configure LO channel
	printf("* Acquiring AD9361 %s lo channel\n", type == TX ? "TX" : "RX");
	if (!get_lo_chan(type, &chn)) { return false; }
	wr_ch_lli(chn, "frequency", cfg->lo_hz);
	return true;
}

/* simple configuration and streaming */
/* usage:
 * Default context, assuming local IIO devices, i.e., this script is run on ADALM-Pluto for example
 $./a.out
 * URI context, find out the uri by typing `iio_info -s` at the command line of the host PC
 $./a.out usb:x.x.x (old instructions)

 	rx_bw_mhz = 2
	rx_sample_rate_mhz = 2.5
	rx_lo_frq_mhz = 2.5
	tx_bw_mhz = 1.5
	tx_sample_rate_mhz 2.5
	tx_lo_frq_mhz 2.5
 
 
 */

// ------------------------
// burst struct
// ------------------------
struct burst_result {
    uint32_t * message;
    size_t size;
};

// ----------------------------------------------------------------
// fill the buffer of a certain size and return it along with the 
// size of the buffer being returned in bytes
// ----------------------------------------------------------------
double complex * get_burst(
					  size_t nrx, 
					  size_t prev_nrx, 
					  struct iio_buffer * rxbuf, 
					  struct iio_channel * rx0_i,
					  struct iio_device * rx
					  ) {
	// start_time = clock();
	ssize_t nbytes_rx;
	void *p_dat, *p_end;
	ptrdiff_t p_inc;

	// Refill RX buffer
	nbytes_rx = iio_buffer_refill(rxbuf);
	if (nbytes_rx < 0) { printf("Error refilling buf %d\n",(int) nbytes_rx); shutdown(); }

	p_inc = iio_buffer_step(rxbuf);
	p_end = iio_buffer_end(rxbuf);

	p_dat = iio_buffer_first(rxbuf, rx0_i);


	printf("\n\n\n\n\nSTART\n\n\n\n\n");


	printf("iio_device_get_sample_size %lu\n", iio_device_get_sample_size(rx));
	nrx += nbytes_rx / iio_device_get_sample_size(rx);
	printf("\tRX %8.2f MSmp accumulated prev rx sample amount %8.2ld \n", nrx/1e6, (nrx - prev_nrx));
    size_t rx_buffer_size = (nrx - prev_nrx);
	printf("\nbuffer size %ld\n", rx_buffer_size);
	double complex* complex_array = (double complex*)malloc(rx_buffer_size * sizeof(double complex));

	int count = 0;
	for (p_dat = iio_buffer_first(rxbuf, rx0_i); p_dat < p_end; p_dat += p_inc) {
		const int16_t i = ((int16_t*)p_dat)[0]; // Real (I)
		const int16_t q = ((int16_t*)p_dat)[1]; // Imag (Q)
		/* Process here */
		// printf("%d %d\n", i, q);
		complex_array[count] = (double)i + (double)q * I;
		count++;
	}

    return complex_array;



}

// ----------------------------------------------------------------
// store data in a complex double array to use in fft
// ----------------------------------------------------------------
double complex * ad9361IQ_to_complex_double(void * pdat, size_t sz) 
{
    // pdat length is sz/2 word length is now 16 bits
    int16_t * data = (int16_t *)pdat;

	int i;

	for (i = 0; i < sz; i++) {
		printf("%i\n", data[i]);
	}

    double  complex * return_data = malloc((sz/2) * sizeof(double complex));

    // sz is size in bytes
    for (i = 0; i < sz / 2; i += 2) {
        return_data[i] = (data[i] + data[i+1]*I);
    }
    return return_data;
}



// ----------------------------------------------------------------
// rxcfg_fs_hz - int in Mhz
// rx_bw_mhz - int in Mhz
// rx_lo_frq_ghz - int in Ghz
// ----------------------------------------------------------------
void init_stream(
	struct iio_device *rx,
	long long rxcfg_fs_hz,
	int rx_bw_mhz,
	int rx_lo_frq_ghz
) {
	// Streaming devices
	// struct iio_device *tx;
	// struct iio_device *rx;

	// RX and TX sample counters
	struct stream_cfg rxcfg;

	// Listen to ctrl+c and IIO_ENSURE
	// signal(SIGINT, handle_sig);
	// RX stream config
	rxcfg.bw_hz = MHZ((long long)rx_bw_mhz);   // 2 MHz rf bandwidth
	rxcfg.fs_hz = MHZ(rxcfg_fs_hz);   // 2.5 MS/s rx sample rate
	rxcfg.lo_hz = GHZ((long long)rx_lo_frq_ghz); // 2.5 GHz rf frequency
	rxcfg.rfport = "A_BALANCED"; // port A (select for rf freq.)
	
	printf("* Acquiring IIO context\n");

	IIO_ENSURE((ctx = iio_create_context_from_uri("usb:3.7.5")) && "No context");


	IIO_ENSURE(iio_context_get_devices_count(ctx) > 0 && "No devices");

	printf("* Acquiring AD9361 streaming devices\n");
	// IIO_ENSURE(get_ad9361_stream_dev(TX, &tx) && "No tx dev found");
	IIO_ENSURE(get_ad9361_stream_dev(RX, &rx) && "No rx dev found");

	printf("* Configuring AD9361 for streaming\n");
	IIO_ENSURE(cfg_ad9361_streaming_ch(&rxcfg, RX, 0) && "RX port 0 not found");
	// IIO_ENSURE(cfg_ad9361_streaming_ch(&txcfg, TX, 0) && "TX port 0 not found");

	printf("* Initializing AD9361 IIO streaming channels\n");
	IIO_ENSURE(get_ad9361_stream_ch(RX, rx, 0, &rx0_i) && "RX chan i not found");
	IIO_ENSURE(get_ad9361_stream_ch(RX, rx, 1, &rx0_q) && "RX chan q not found");


	printf("* Enabling IIO streaming channels\n");
	iio_channel_enable(rx0_i);
	iio_channel_enable(rx0_q);
}



// ----------------------------------------------------------------
// loop and every loop fill the rx buffer with required data 
// at such a sample rate 
// ----------------------------------------------------------------
double complex * stream_iq (
	struct iio_device *rx)
{

	size_t nrx = 0;
    size_t prev_nrx = 0;
	// Stream configurations


	// printf("* Starting IO streaming (press CTRL+C to cancel)\n");

    double complex* complex_array = get_burst(nrx, prev_nrx, rxbuf, rx0_i, rx);

	// printf("\n\n\n");
	
	// shutdown();
	return complex_array;
}


#define COEFFICIENT_COUNT 102

// lowpass filter
static double h[COEFFICIENT_COUNT] = {
  0.20957043, -0.02839557, -0.02680268, -0.02553468, -0.02445035, -0.02363894,
 -0.02292124, -0.0224123 , -0.02193533, -0.02161248, -0.02125591, -0.02101173,
 -0.02065006, -0.02038014, -0.01991693, -0.01954874, -0.01877648, -0.01843097,
 -0.01745993, -0.01629491, -0.01545344, -0.01427821, -0.01294396, -0.01132806,
 -0.00960659, -0.00771314, -0.00573805, -0.00361847, -0.00140219,  0.00092767,
  0.00335111,  0.0058906 ,  0.00850051,  0.01112743,  0.01382576,  0.01656615,
  0.01912009,  0.02177714,  0.02432297,  0.02674758,  0.02901337,  0.03118255,
  0.03320429,  0.03507714,  0.03673496,  0.03818105,  0.039403  ,  0.04042112,
  0.04116095,  0.04164047,  0.04192285,  0.04192285,  0.04164047,  0.04116095,
  0.04042112,  0.039403  ,  0.03818105,  0.03673496,  0.03507714,  0.03320429,
  0.03118255,  0.02901337,  0.02674758,  0.02432297,  0.02177714,  0.01912009,
  0.01656615,  0.01382576,  0.01112743,  0.00850051,  0.0058906 ,  0.00335111,
  0.00092767, -0.00140219, -0.00361847, -0.00573805, -0.00771314, -0.00960659,
 -0.01132806, -0.01294396, -0.01427821, -0.01545344, -0.01629491, -0.01745993,
 -0.01843097, -0.01877648, -0.01954874, -0.01991693, -0.02038014, -0.02065006,
 -0.02101173, -0.02125591, -0.02161248, -0.02193533, -0.0224123 , -0.02292124,
 -0.02363894, -0.02445035, -0.02553468, -0.02680268, -0.02839557,  0.20957043
};




int main() {

	struct iio_device *rx;
	// init_stream(rx, 2.5, 5, 2.5);
	struct stream_cfg rxcfg;

	// Listen to ctrl+c and IIO_ENSURE
	// signal(SIGINT, handle_sig);
	// RX stream config
	rxcfg.bw_hz = MHZ((long long)5);   // 2 MHz rf bandwidth
	rxcfg.fs_hz = MHZ(2.5);   // 2.5 MS/s rx sample rate
	rxcfg.lo_hz = GHZ((long long)2.5); // 2.5 GHz rf frequency
	rxcfg.rfport = "A_BALANCED"; // port A (select for rf freq.)
	
	printf("* Acquiring IIO context\n");

	IIO_ENSURE((ctx = iio_create_context_from_uri("usb:3.7.5")) && "No context");


	IIO_ENSURE(iio_context_get_devices_count(ctx) > 0 && "No devices");

	printf("* Acquiring AD9361 streaming devices\n");
	// IIO_ENSURE(get_ad9361_stream_dev(TX, &tx) && "No tx dev found");
	IIO_ENSURE(get_ad9361_stream_dev(RX, &rx) && "No rx dev found");

	printf("* Configuring AD9361 for streaming\n");
	IIO_ENSURE(cfg_ad9361_streaming_ch(&rxcfg, RX, 0) && "RX port 0 not found");
	// IIO_ENSURE(cfg_ad9361_streaming_ch(&txcfg, TX, 0) && "TX port 0 not found");

	printf("* Initializing AD9361 IIO streaming channels\n");
	IIO_ENSURE(get_ad9361_stream_ch(RX, rx, 0, &rx0_i) && "RX chan i not found");
	IIO_ENSURE(get_ad9361_stream_ch(RX, rx, 1, &rx0_q) && "RX chan q not found");


	printf("* Enabling IIO streaming channels\n");
	iio_channel_enable(rx0_i);
	iio_channel_enable(rx0_q);
	
	printf("* Creating non-cyclic IIO buffers with 1 MiS\n");
	rxbuf = iio_device_create_buffer(rx, 4096, false);
	if (!rxbuf) {
		perror("Could not create RX buffer");
		shutdown();
	}
	while(1){
    	// Input sequences
    	double complex * x = stream_iq(rx);

    	double complex y[N + M - 1];
    	// Create FFTW plans
    	fftw_complex* x_freq = fftw_malloc(sizeof(fftw_complex) * (N + M - 1));
    	fftw_complex* h_freq = fftw_malloc(sizeof(fftw_complex) * (N + M - 1));
    	fftw_complex* y_freq = fftw_malloc(sizeof(fftw_complex) * (N + M - 1));


    	fftw_plan x_plan = fftw_plan_dft_1d(N + M - 1, x_freq, x_freq, FFTW_FORWARD, FFTW_ESTIMATE);
    	fftw_plan h_plan = fftw_plan_dft_1d(N + M - 1, h_freq, h_freq, FFTW_FORWARD, FFTW_ESTIMATE);
    	fftw_plan y_plan = fftw_plan_dft_1d(N + M - 1, y_freq, y_freq, FFTW_BACKWARD, FFTW_ESTIMATE);

    	// Perform FFT on input sequences
		int count = 0;
    	for (int i = 0; i < N; i++) {
    	    x_freq[i] = x[i];

    	}

    	fftw_execute(x_plan);

    	for (int i = 0; i < M; i++) {
    	    h_freq[i] = h[i];
    	}
    	fftw_execute(h_plan);

    	// Perform element-wise multiplication in the frequency domain
    	for (int i = 0; i < N + M - 1; i++) {
    	    y_freq[i] = x_freq[i] * h_freq[i];
    	}

    	// Perform inverse FFT to obtain the convolution result
    	fftw_execute(y_plan);

    	// Normalize the result by scaling
    	for (int i = 0; i < N + M - 1; i++) {
    	    y[i] = y_freq[i] / (N + M - 1);
    	}

    	// Print the convolution result
    	printf("Convolution result:\n");
    	for (int i = 0; i < N + M - 1; i++) {
    	    printf("%.2f + %.2fi\n", creal(y[i]), cimag(y[i]));
    	}

    	// Destroy FFTW plans and free allocated memory
    	fftw_destroy_plan(x_plan);
    	fftw_destroy_plan(h_plan);
    	fftw_destroy_plan(y_plan);
    	fftw_free(x_freq);
    	fftw_free(h_freq);
    	fftw_free(y_freq);
	}

    return 0;
}
