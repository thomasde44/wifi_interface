#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>
#include <stddef.h>

// Define the squelch threshold level
const float SQUELCH_THRESHOLD = -50.0;

double set_threshold(double decibels)
{
    // convert to absolute threshold (mag squared)
    return powf(10.0, decibels / 10);
}


// Squelch function
void applySquelch(const int16_t* inputSignal, size_t numSamples, int16_t* outputSignal) {
    for (size_t i = 0; i < numSamples; ++i) {
        // Calculate the power of the current sample
        float power = powf(inputSignal[i], 2.0);

        // Check if the power is above the threshold
        if (power > SQUELCH_THRESHOLD) {
            // Signal above the squelch threshold, copy to output
            outputSignal[i] = inputSignal[i];
        } else {
            // Signal below the squelch threshold, mute or discard
            outputSignal[i] = 0;
        }
    }
}


int main() {
    printf("%f\n", set_threshold(-50));
}