#include <stdio.h>
#include <pcap.h>
#include <time.h>
#include <netinet/ip.h> // For IP header structure
#include <netinet/ether.h> // For Ethernet header structure
#include <netinet/tcp.h>
#include <netinet/udp.h> // For TCP header structure
#include <stdlib.h>
#include <string.h>


// -------------------------------------------
// ethernet packets come with a timestamp
// print it out in a readable format
// -------------------------------------------
void print_time(struct timeval ts){

    gettimeofday(&ts, NULL); // Get current timestamp

    time_t timestamp = ts.tv_sec; // Extract seconds from timeval
    struct tm *timeinfo;
    timeinfo = localtime(&timestamp);
    char buffer[30];
    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeinfo); // Format the timestamp

    printf("Timestamp: %s.%06ld\n", buffer, ts.tv_usec);
}


// -------------------------------------------
// return the source ip address from incoming packet
// -------------------------------------------
char * return_src_ip(const u_char * packet) {
    struct ether_header * eth_header = (struct ether_header *)packet; // Ethernet header
    struct ip * ip_header = (struct ip *)(packet + sizeof(struct ether_header)); // IP header

    char src_ip[INET_ADDRSTRLEN];

    // Convert source IP address to string
    inet_ntop(AF_INET, &(ip_header->ip_src), src_ip, INET_ADDRSTRLEN);

    char* src_ip_str = (char*)malloc(INET_ADDRSTRLEN * sizeof(char));
    if (src_ip_str == NULL) {
        fprintf(stderr, "Failed to allocate memory for destination IP address.\n");
        return NULL;
    }

    strcpy(src_ip_str, src_ip);
    return src_ip_str;
}

// -------------------------------------------
// return the destination ip address from incoming packet
// -------------------------------------------
char * return_dst_ip(const u_char * packet) {
    struct ether_header * eth_header = (struct ether_header *)packet; // Ethernet header
    struct ip * ip_header = (struct ip *)(packet + sizeof(struct ether_header)); // IP header

    char dst_ip[INET_ADDRSTRLEN];

    // Convert destination IP address to string
    inet_ntop(AF_INET, &(ip_header->ip_dst), dst_ip, INET_ADDRSTRLEN);

    char* dst_ip_str = (char*)malloc(INET_ADDRSTRLEN * sizeof(char));
    if (dst_ip_str == NULL) {
        fprintf(stderr, "Failed to allocate memory for destination IP address.\n");
        return NULL;
    }

    strcpy(dst_ip_str, dst_ip);
    return dst_ip_str;
}

/* 
----------------------------------------------------
// process the received ethernet packet

 struct pcap_pkthdr {
      struct timeval ts;  
      bpf_u_int32 caplen; 
      bpf_u_int32 len;    
 };


----------------------------------------------------
*/
void packet_handler(u_char *user_data, const struct pcap_pkthdr *pkthdr, const u_char *packet) {
    // Process the packet data here
    printf("\nReceived a packet!\n");
    // printf("len %d\n", pkthdr->len);
    print_time(pkthdr->ts);

    printf("Source IP: %s\n", return_src_ip(packet));
    printf("Destination IP: %s\n", return_dst_ip(packet));

    struct ether_header * eth_header = (struct ether_header *)packet; // Ethernet header
    struct ip * ip_header = (struct ip *)(packet + sizeof(struct ether_header)); // IP header

  
    unsigned short src_port;
    unsigned short dst_port;
    switch (ip_header->ip_p) {
        case IPPROTO_IP:
            printf("IPPROTO_IP");
            break;
        case IPPROTO_IPV6:
            printf("IPPROTO_IPV6");
            break;
        case IPPROTO_ICMP:
            printf("IPPROTO_ICMP");
            break;
        case IPPROTO_RAW:
            printf("IPPROTO_RAW");
            break;
        case IPPROTO_TCP:
            printf("IPPROTO_TCP");
            struct tcphdr * tcp_header = (struct tcphdr *)(packet + sizeof(struct ether_header) + sizeof(struct ip)); // TCP header
            // Calculate the size of the TCP header
            int tcp_header_size = tcp_header->th_off * 4;
            src_port = ntohs(tcp_header->source);
            dst_port = ntohs(tcp_header->dest);
            // Calculate the size of the payload
            int payload_size = pkthdr->len - (sizeof(struct ether_header) + sizeof(struct ip) + tcp_header_size);
            // Access the payload data
            const u_char *payload = packet + sizeof(struct ether_header) + sizeof(struct ip) + tcp_header_size;
            // Process the payload data here
            printf("\nReceived a TCP packet with payload size: %d dst port: %d src port %d", payload_size, dst_port, src_port);
            break;
        case IPPROTO_UDP:
            printf("IPPROTO_UDP");
            struct udphdr * udp_header  = (struct udphdr *)(packet + sizeof(struct ether_header) + sizeof(struct ip));
            src_port = ntohs(udp_header->uh_sport);
            dst_port = ntohs(udp_header->uh_dport);
            printf("\nReceived a UDP packet dst port: %d src port %d", dst_port, src_port);
            break;
        default:
            printf("default");
            break;
    }

    // for (int i = 0; i < pkthdr->len; i++) {
    //     u_char byte = *(packet + i);
    //     printf("%d ", byte);
    // }
    printf("\n");
}

// -------------------------------------------
// print interfaces on machine
// -------------------------------------------
int print_interfaces() {
    pcap_if_t* interfaces;
    pcap_if_t* interface;
    char errbuf[PCAP_ERRBUF_SIZE];
    int ret;

    // Retrieve the list of available network interfaces
    ret = pcap_findalldevs(&interfaces, errbuf);
    if (ret == -1) {
        printf("Error finding network interfaces: %s\n", errbuf);
        return 1;
    }

    // Print the available network interfaces
    printf("Available network interfaces:\n");
    for (interface = interfaces; interface != NULL; interface = interface->next) {
        printf("- %s\n", interface->name);
    }

    // Free the memory allocated for the network interface list
    pcap_freealldevs(interfaces);
    return 0;
}


int main() {
    pcap_t *handle;
    char errbuf[PCAP_ERRBUF_SIZE];
    struct pcap_pkthdr header;
    const u_char *packet;
    char *dev;

    // for use with find all devs
    pcap_if_t* interfaces;
    int ret;

    print_interfaces();
    // Get the name of the WiFi interface
    // dev = pcap_lookupdev(errbuf);
    // if (dev == NULL) {
    //     printf("Error finding WiFi interface: %s\n", errbuf);
    //     return 1;
    // }

    ret = pcap_findalldevs(&interfaces, errbuf);
    if (ret == -1) {
        printf("Error finding network interfaces: %s\n", errbuf);
        return 1;
    }
    // first value in interfaces list is dev you want
    dev = interfaces->name; // *(interfaces + 0)->name
    printf("Listening on device: %s\n", interfaces->name);

    // Open the WiFi interface for capturing
    handle = pcap_open_live(dev, BUFSIZ, 1, 2048, errbuf);
    if (handle == NULL) {
        printf("Error opening device: %s\n", errbuf);
        return 1;
    }

    // Start capturing packets
    pcap_loop(handle, 0, packet_handler, NULL);

    // Close the capture handle
    pcap_close(handle);

    return 0;
}
