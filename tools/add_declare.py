

import re
import numpy as np
import pandas as pd


import os

def process_file(file_path):
    with open(file_path, 'r+') as file:
        lines = file.readlines()
        modified_lines = []

        # Find the index where the headers end
        header_end_index = -1
        for i, line in enumerate(lines):
            if line.startswith('#include'):
                header_end_index = i + 1

        if header_end_index != -1:
            # Add the desired code after the header includes
            modified_lines.extend(lines[:header_end_index])
            modified_lines.append("#if defined(__CUDA_CODE__) || defined(__cplusplus)\n")
            modified_lines.append("extern \"C\" {\n")
            modified_lines.append("#endif\n\n")
            modified_lines.extend(lines[header_end_index:])

            # Write the modified content back to the file
            file.seek(0, 0)
            file.writelines(modified_lines)
            file.truncate()

def process_file_reverse(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()

    modified_lines = []
    inside_block = False

    for line in reversed(lines):
        if line.strip() == "#endif":
            modified_lines.append(line)
            modified_lines.append("#if defined(__CUDA_CODE__) && defined(__cplusplus)\n")
            modified_lines.append("}\n")
            modified_lines.append("#endif\n")
            inside_block = True
        else:
            modified_lines.append(line)

    # modified_lines.reverse()

    with open(file_path, 'w') as file:
        file.writelines(modified_lines)

def search_files(directory, extensions):
    for root, dirs, files in os.walk(directory):
        for file in files:
            for extension in extensions:
                if file.endswith(extension):
                    print(file)
                    file_path = os.path.join(root, file)
                    # process_file_reverse(file_path)
                    process_file(file_path)
                    # remove_added_code(file_path)

# Example usage

search_directory = "/home/meanthatmuch/wifi_interface/application/ports/posix-qv"
file_extensions = [".h", ".c", ".cu"]
search_files(search_directory, file_extensions)

