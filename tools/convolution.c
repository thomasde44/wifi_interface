#include <stdio.h>
#include <stdlib.h>
#include <complex.h>

#include <cuda_runtime.h>
#include <cufft.h>
#include <cufftXt.h>

void convolutionDS(complex double* x, complex double* h, int x_length, int h_length) {
    complex double* z = (complex double*)malloc(x_length * h_length * sizeof(complex double));
    int i, j;

    // Initialize z with zeros
    for (i = 0; i < x_length; i++) {
        for (j = 0; j < h_length; j++) {
            z[i * h_length + j] = 0.0 + 0.0 * I;
        }
    }

    // Assign x to the first column of z
    for (i = 0; i < x_length; i++) {
        z[i * h_length] = x[i];
    }

    // Perform circular shifts and compute the convolution
    for (i = 1; i < h_length; i++) {
        for (j = 0; j < x_length; j++) {
            z[j * h_length + i] = x[(j + (i - 1)) % x_length];
        }
    }

    // Compute the convolution ansr = z * h
    complex double* ansr = (complex double*)malloc(x_length * sizeof(complex double));
    for (i = 0; i < x_length; i++) {
        ansr[i] = 0.0 + 0.0 * I;
        for (j = 0; j < h_length; j++) {
            ansr[i] += z[i * h_length + j] * h[j];
        }
    }



    // Print the result
    for (i = 0; i < x_length; i++) {
        printf("%f + %fi \n", creal(ansr[i]), cimag(ansr[i]));
    }
    printf("\n");

    // Free allocated memory
    free(z);
    free(ansr);
}

int main() {
    // Example usage
    complex double x[] = {1.0 + 2.0 * I, 2.0 + 1.0 * I, 3.0 - 1.0 * I, 4.0 + 3.0 * I, 5.0 - 2.0 * I, 9.0 - 3.8*I};
    complex double h[] = {0.5 + 0.2 * I, 0.25 - 0.1 * I, 0.125 + 0.1*I};
    double complex coefficients[] = {
        1.00000000     + 0.00000000*I,    -4.31774144e-01 - 1.33028193e-02*I,
        3.05481247e-03 + 1.88414384e-04*I, 1.08539571e-03 + 1.00576725e-04*I,
        5.67492960e-04 + 7.02708571e-05*I, 3.50258230e-04 + 5.43701345e-05*I,
        2.37627417e-04 + 4.44202627e-05*I, 1.71453978e-04 + 3.75490006e-05*I,
        1.29187181e-04 + 3.24918802e-05*I, 1.00515918e-04 + 2.85992443e-05*I,
        8.01606000e-05 + 2.55009227e-05*I, 6.51831982e-05 + 2.29696728e-05*I,
        5.38404308e-05 + 2.08578940e-05*I, 4.50437138e-05 + 1.90653688e-05*I,
        3.80843253e-05 + 1.75215699e-05*I, 3.24845287e-05 + 1.61753733e-05*I,
        2.79126675e-05 + 1.49887859e-05*I, 2.41326042e-05 + 1.39329656e-05*I,
        2.09724818e-05 + 1.29856109e-05*I, 1.83047968e-05 + 1.21291983e-05*I,
        1.60333322e-05 + 1.13497609e-05*I, 1.40843737e-05 + 1.06360253e-05*I,
        1.24006774e-05 + 9.97878799e-06*I, 1.09372448e-05 + 9.37045766e-06*I,
        9.65831109e-06 + 8.80471393e-06*I, 8.53516489e-06 + 8.27624925e-06*I,
        7.54454630e-06 + 7.78057122e-06*I, 6.66745460e-06 + 7.31384929e-06*I,
        5.88825013e-06 + 6.87279485e-06*I, 5.19397010e-06 + 6.45456620e-06*I,
        4.57380210e-06 + 6.05669281e-06*I, 4.01867521e-06 + 5.67701429e-06*I,
        3.52093948e-06 + 5.31363083e-06*I, 3.07411285e-06 + 4.96486273e-06*I,
        2.67267969e-06 + 4.62921702e-06*I, 2.31192939e-06 + 4.30535981e-06*I,
        1.98782621e-06 + 3.99209318e-06*I, 1.69690373e-06 + 3.68833582e-06*I,
        1.43617884e-06 + 3.39310659e-06*I, 1.20308120e-06 + 3.10551057e-06*I,
        9.95395438e-07 + 2.82472714e-06*I, 8.11213292e-07 + 2.54999966e-06*I,
        6.48894204e-07 + 2.28062657e-06*I, 5.07032658e-07 + 2.01595350e-06*I,
        3.84431195e-07 + 1.75536649e-06*I, 2.80078158e-07 + 1.49828581e-06*I,
        1.93129431e-07 + 1.24416048e-06*I, 1.22893595e-07 + 9.92463345e-07*I,
        6.88200368e-08 + 7.42686475e-07*I, 3.04896610e-08 + 4.94336976e-07*I,
        7.60792468e-09 + 2.46933007e-07*I, 1.93792668e-17 - 4.65910473e-18*I,
        7.60792468e-09 - 2.46933007e-07*I, 3.04896610e-08 - 4.94336976e-07*I,
        6.88200368e-08 - 7.42686475e-07*I, 1.22893595e-07 - 9.92463345e-07*I,
        1.93129431e-07 - 1.24416048e-06*I, 2.80078158e-07 - 1.49828581e-06*I,
        3.84431195e-07 - 1.75536649e-06*I, 5.07032658e-07 - 2.01595350e-06*I,
        6.48894204e-07 - 2.28062657e-06*I, 8.11213292e-07 - 2.54999966e-06*I,
        9.95395438e-07 - 2.82472714e-06*I, 1.20308120e-06 - 3.10551057e-06*I,
        1.43617884e-06 - 3.39310659e-06*I, 1.69690373e-06 - 3.68833582e-06*I,
        1.98782621e-06 - 3.99209318e-06*I, 2.31192939e-06 - 4.30535981e-06*I,
        2.67267969e-06 - 4.62921702e-06*I, 3.07411285e-06 - 4.96486273e-06*I,
        3.52093948e-06 - 5.31363083e-06*I, 4.01867521e-06 - 5.67701429e-06*I,
        4.57380210e-06 - 6.05669281e-06*I, 5.19397010e-06 - 6.45456620e-06*I,
        5.88825013e-06 - 6.87279485e-06*I, 6.66745460e-06 - 7.31384929e-06*I,
        7.54454630e-06 - 7.78057122e-06*I, 8.53516489e-06 - 8.27624925e-06*I,
        9.65831109e-06 - 8.80471393e-06*I, 1.09372448e-05 - 9.37045766e-06*I,
        1.24006774e-05 - 9.97878799e-06*I, 1.40843737e-05 - 1.06360253e-05*I,
        1.60333322e-05 - 1.13497609e-05*I, 1.83047968e-05 - 1.21291983e-05*I,
        2.09724818e-05 - 1.29856109e-05*I, 2.41326042e-05 - 1.39329656e-05*I,
        2.79126675e-05 - 1.49887859e-05*I, 3.24845287e-05 - 1.61753733e-05*I,
        3.80843253e-05 - 1.75215699e-05*I, 4.50437138e-05 - 1.90653688e-05*I,
        5.38404308e-05 - 2.08578940e-05*I, 6.51831982e-05 - 2.29696728e-05*I,
        8.01606000e-05 - 2.55009227e-05*I, 1.00515918e-04 - 2.85992443e-05*I,
        1.29187181e-04 - 3.24918802e-05*I, 1.71453978e-04 - 3.75490006e-05*I,
        2.37627417e-04 - 4.44202627e-05*I, 3.50258230e-04 - 5.43701345e-05*I,
        5.67492960e-04 - 7.02708571e-05*I, 1.08539571e-03 - 1.00576725e-04*I,
        3.05481247e-03 - 1.88414384e-04*I,-4.31774144e-01 - 1.33028193e-02*I
    };

    convolutionDS(x, coefficients, 6, 102);
}