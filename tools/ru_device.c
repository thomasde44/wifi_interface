// #include <iio.h>
// #include <time.h>
// #include <string.h>
// #include <stdbool.h>
// #include <stdint.h>
// #include <signal.h>
// #include <stdio.h>
// #include <errno.h>
#include "ru_device.h"


// ****************************************************************************
// frequency  		-  LO freq
// samplerate 		-  data processing sample rate
// bandwidth  		-  RX bandwidth
// quadrature 		-  demod boolean
// RF dc correction -  on or off
// bb dc correction -  on or off
// gain             -  can be hybrid etc
// gain_value		-  magnitude of selected gain type
// filter			-  file to implement filter (not supported currently)
// auto_filter		-  auto filter on or off
// ****************************************************************************
bool enable_and_set_params(unsigned long long frequency,
		unsigned long samplerate,
		unsigned long bandwidth,
		bool quadrature, bool rfdc, bool bbdc,
		const char *gain, double gain_value,
		const char *filter, bool auto_filter)
{
		int ret = 0;

		if (!ctx)
		{
		    printf("Unable to create context");
		}
	    dev = iio_context_find_device(ctx, "cf-ad9361-lpc");
	    phy = iio_context_find_device(ctx, "ad9361-phy");
		
		if (!dev || !phy) {
			printf("error getting dev or phy context");
		    return false;
	    }

		unsigned int nb_channels, i;
	    /* First disable all channels */
	    nb_channels = iio_device_get_channels_count(dev);
	    for (i = 0; i < nb_channels; i++)
		{
		    iio_channel_disable(iio_device_get_channel(dev, i));
		}

		if (filter && filter[0])
		{
		    auto_filter = false;
		}

		if (auto_filter) 
		{
			struct iio_channel *chan = NULL;

			const char *attr = NULL;
			char str[32];
			memset(str, 0, sizeof(str));
			// -------------------------------------------
			ret = iio_device_identify_filename(phy,
				    "out_altvoltage0_RX_LO_frequency", &chan, &attr);
		    if (ret) 
			{
			    printf("Parameter not recognized out_altvoltage0_RX_LO_frequency -- enable_and_set_params()\n");
			    return false;
		    }
			else
			{
				snprintf(str, sizeof(str), "%llu", frequency);
				if (!process_param(chan, attr, str))
				{
					printf("error process param: out_altvoltage0_RX_LO_frequency\n");
					return false;
				}
			}
			memset(str, 0, sizeof(str));
			// -------------------------------------------
			ret = iio_device_identify_filename(phy,
				    "in_voltage_rf_bandwidth", &chan, &attr);
		    if (ret) 
			{
			    printf("Parameter not recognized in_voltage_rf_bandwidth -- enable_and_set_params()\n");
			    return false;
		    }
			else
			{
				snprintf(str, sizeof(str), "%lu", bandwidth);
				if (!process_param(chan, attr, str))
				{
					printf("error process param: in_voltage_rf_bandwidth");
					return false;
				}
			}

			memset(str, 0, sizeof(str));
			// -------------------------------------------
			ret = iio_device_identify_filename(phy,
				    "in_voltage_quadrature_tracking_en", &chan, &attr);
		    if (ret)
			{
			    printf("Parameter not recognized in_voltage_quadrature_tracking_en -- enable_and_set_params()\n");
			    return false;
		    }
			else
			{
				snprintf(str, sizeof(str), "%d", quadrature);
				if (!process_param(chan, attr, str))
				{
					printf("error process param: in_voltage_quadrature_tracking_en");
					return false;
				}
			}
			memset(str, 0, sizeof(str));
			// -------------------------------------------
			ret = iio_device_identify_filename(phy,
				    "in_voltage_rf_dc_offset_tracking_en", &chan, &attr);
		    if (ret)
			{
			    printf("Parameter not recognized in_voltage_rf_dc_offset_tracking_en -- enable_and_set_params()\n");
			    return false;
		    }
			else
			{
				snprintf(str, sizeof(str), "%d", rfdc);
				if (!process_param(chan, attr, str))
				{
					printf("error process param: in_voltage_rf_dc_offset_tracking_en");
					return false;
				}
			}
			memset(str, 0, sizeof(str));
			// -------------------------------------------
			ret = iio_device_identify_filename(phy,
				    "in_voltage_bb_dc_offset_tracking_en", &chan, &attr);
		    if (ret)
			{
			    printf("Parameter not recognized in_voltage_bb_dc_offset_tracking_en -- enable_and_set_params()\n");
			    return false;
		    }
			else
			{
				snprintf(str, sizeof(str), "%d", bbdc);
				if (!process_param(chan, attr, str))
				{
					printf("error process param: in_voltage_bb_dc_offset_tracking_en");
					return false;
				}
			}
			memset(str, 0, sizeof(str));
			// -------------------------------------------
			ret = iio_device_identify_filename(phy,
				    "in_voltage0_gain_control_mode", &chan, &attr);
		    if (ret)
			{
			    printf("Parameter not recognized in_voltage0_gain_control_mode -- enable_and_set_params()\n");
			    return false;
		    }
			else
			{
				if (!process_param(chan, attr, gain))
				{
					printf("error process param: in_voltage0_gain_control_mode");
					return false;
				}
			}
			// -------------------------------------------
			ret = iio_device_identify_filename(phy,
				    "in_voltage0_rf_port_select", &chan, &attr);
		    if (ret)
			{
			    printf("Parameter not recognized in_voltage0_rf_port_select -- enable_and_set_params()\n");
			    return false;
		    }
			else
			{
				if (!process_param(chan, attr, "A_BALANCED"))
				{
					printf("error process: in_voltage0_rf_port_select");
					return false;
				}
			}

			ret = ad9361_set_bb_rate(phy, samplerate);
		    if (ret) {
			    printf("Unable to set BB rate -- enable_and_set_params()");
				return false; 
		    }
			else
			{
				printf("set bb sample rate\n");
			}
	    }
		else
		{
			printf("Not defining filter currently -- enable_and_set_params()\n");
		}

		ret = enable_rx_channel(&rx0_i, pluto_channels[0]);

		if (!ret)
		{
			printf("channel enable fail\n");
			return false;
		}
		ret = enable_rx_channel(&rx0_q, pluto_channels[1]);

		if (!ret)
		{
			printf("channel enable fail\n");
			return false;
		}

		return true;
}

// ****************************************************************************
// unsigned int _buffer_size - byte size for rx channel reads
// ****************************************************************************
bool set_buffer_size(unsigned int update_buffer_size)
{
	// pthread_mutex_lock(&io_mutex);
	if (buffer_size != update_buffer_size) {

		rxbuf = iio_device_create_buffer(dev, update_buffer_size, false);
		if (!rxbuf)
		{
			printf("Unable to create buffer! -- set_buffer_size()\n");
			return false;
		}
	}

	buffer_size = update_buffer_size;
	return true;
}

// ****************************************************************************
// const struct iio_channel *chn - channel you want to read from
// void *dst - where to write the read bytes to
// size_t len - size of bytes to read from channel (bytes)
// ****************************************************************************
void channel_read(const struct iio_channel *chn,
		    void *dst, size_t len)
{
	uintptr_t src_ptr, dst_ptr = (uintptr_t) dst, end = dst_ptr + len;
	unsigned int length = iio_channel_get_data_format(chn)->length / 8;
	uintptr_t buf_end = (uintptr_t) iio_buffer_end(rxbuf);

	ptrdiff_t buf_step = iio_buffer_step(rxbuf) * (decimation + 1);
	for (src_ptr = (uintptr_t) iio_buffer_first(rxbuf, chn) + byte_offset;
			src_ptr < buf_end && dst_ptr + length <= end;
			src_ptr += buf_step, dst_ptr += length)
		iio_channel_convert(chn,
				(void *) dst_ptr, (const void *) src_ptr);
}

// read data from the trx libiio buffer
void * refill_buffer_thread_trx(void * arg)
{

	// arg is of type thrd_arg_msg_q to pass the mq
	struct thrd_arg_msg_q *thread_args = (struct thrd_arg_msg_q *)arg;
	mqd_t mq = thread_args->mq1;
	
	size_t nbytes_rx;
	char *p_dat, *p_end;
	char p_dat_recv;
	ptrdiff_t p_inc;

	while(1)
	{
		// printf("in refill_buffer_thread_trx\n");
		pthread_mutex_lock(&iio_mutex);
		nbytes_rx = iio_buffer_refill(rxbuf);
		// p_inc = iio_buffer_step(rxbuf);
		pthread_mutex_unlock(&iio_mutex);
		if (nbytes_rx < 0) {
		    printf("Error refilling buf %ld\n", (long)nbytes_rx);
		    perror("iio_buffer_refill error");
		    shutdown();
		}
		// READ: Get pointers to RX buf and read IQ from RX buf port 0

		p_dat = iio_buffer_first(rxbuf, rx0_i);

		size_t sz = nbytes_rx/(sizeof(int));

    	if (p_dat != NULL) 
		{
			struct rx_message* input_data = create_rx_message(sz);

			memcpy(input_data->data, p_dat, sz);
			// printf("sz: %u\n", (unsigned int)sz);
			ptrdiff_t step_size = 4;

			input_data->size = sz;

			if (send_message((void*)input_data, mq))
			{
				// continue msg sent
				printf("buf sent rx_queue\n");
			}
			else
			{
				printf("not sent\n");
			}
		}
	}
	return NULL;
}
// every 4 bytes from iio_biffer_first is 16 bit I and 16 bit Q
// sends the data to work queue
void * read_latest_message_trx(void * arg)
{
	// printf("in read_latest_message_trx\n");
	struct thrd_arg_msg_q *thread_args = (struct thrd_arg_msg_q *)arg;
	mqd_t mq1 = thread_args->mq1; // iq data mq_trx
	mqd_t mq2 = thread_args->mq2; // iq data read it and process mq_work

	struct mq_attr attr;
    if (mq_getattr(mq1, &attr) == -1) {
        perror("mq_getattr error");
        return false; // Return false for failure
    }
	// loop indefinitely pulling data from the rx antenna
	while(1)
	{
		// printf("in read_latest_message_trx\n");
		// to holdantenna output data
		struct rx_message* output_data = create_rx_message(attr.mq_msgsize);

		// read from the queue
		int read_eval = receive_message(mq1, output_data, 0);
		if (!read_eval) 
		{
			printf("error reading message\n");
			exit(1);
		}
		// create message to send to the work
		struct rx_message* input_data = create_rx_message(attr.mq_msgsize);
		// output_data is from antenna
		// input goes to queue of mq2

		memcpy(input_data->data, output_data->data, attr.mq_msgsize);
		// send message
		if (send_message((void*)input_data, mq2))
		{
			// continue msg sent
			printf("buf sent cuda_queue\n");
		}
		else
		{
			printf("not sent\n");
		}
	}
}


void * read_latest_message(void * arg)
{
	// printf("in read_latest_message\n");
	struct thrd_arg_msg_q *thread_args = (struct thrd_arg_msg_q *)arg;
	mqd_t mq1 = thread_args->mq1;

	
	struct mq_attr attr;
    if (mq_getattr(mq1, &attr) == -1) {
        perror("mq_getattr error");
        return false; // Return false for failure
    }

	while(1)
	{
		// printf("in read_latest_message\n");
		struct rx_message* output_data = create_rx_message(attr.mq_msgsize);


		int read_eval = receive_message(mq1, output_data, 0);
		if (!read_eval) 
		{
			printf("error reading message\n");
			exit(1);
		}
	}
}

void * read_latest_work(void * arg)
{

	struct thrd_arg_msg_q *thread_args = (struct thrd_arg_msg_q *)arg;
	mqd_t mq1 = thread_args->mq1;
	mqd_t mq2 = thread_args->mq2;
	
	struct mq_attr attr;
    if (mq_getattr(mq1, &attr) == -1) {
        perror("mq_getattr error");
        return false; // Return false for failure
    }
	double complex x[N];
	double complex * y;
	// double complex * y_tmp = (double complex *)malloc((N + M - 1) * sizeof(double complex));

	while(1)
	{

		struct rx_message* output_data = create_rx_message(attr.mq_msgsize);

		int read_eval = receive_message(mq1, output_data, 0);
		if (!read_eval) 
		{
			printf("error reading message\n");
			exit(1);
		}
		else
		{
			printf("read cuda_queue\n");
		}
		int sz = attr.mq_msgsize/4;
		ptrdiff_t step_size = 4;
		char * buf_data = (char *)output_data->data;
		char * buf_end = buf_data + attr.mq_msgsize;
		char * p_dat;
		memset(x, 0, sizeof(x));
		int index = 0;
		while(buf_data + step_size <= buf_end)
		{
			int16_t* p_dat = (int16_t*)buf_data;
			const int16_t i = p_dat[0];
        	const int16_t q = p_dat[1];

			x[index] = (double complex)i + (double complex)q * I;

			buf_data += step_size;
			index++;
		}

		y = lowpass_fft(x);

		// for (int i = 0; i < process_size; i++)
		// {
		// 	printf("%d y: %.8f + %.8fi\n", i, creal(y[i]), cimag(y[i]));
		// }
		// printf("read cuda_queue4\n");
		double complex * y_squelch = squelch(-50.0, 1.0, y);
		// for (int i = 0; i < process_size; i++)
		// {
		// 	printf("%d y_squelch: %.8f + %.8fi\n", i, creal(y_squelch[i]), cimag(y_squelch[i]));
		// }
		float * y_demod = quadrature_demod(y_squelch);
		// for (int i = 0; i < process_size; i++)
		// {
		// 	printf("%d y_demod: %.8f\n", i, y_demod[i]);
		// }
		// create message to send to the work
		struct rx_message* input_data = create_rx_message(attr.mq_msgsize);
		// output_data is from antenna
		// input goes to queue of mq2
		memcpy(input_data->data, (void*)y_demod, process_size);
		// send message
		// for (int i = 0; i < process_size; i++)
		// {
		// 	printf("%d y_demod: %.8f %ld %d\n", i, y_demod[i], attr.mq_msgsize, process_size);
		// }
		if (send_message((void*)input_data, mq2))
		{
			// continue msg sent
			printf("buf sent cuda_queue\n");
		}
		else
		{
			printf("not sent\n");
		}
	}
}

void * read_latest_audio(void * arg)
{
	struct thrd_arg_msg_q *thread_args = (struct thrd_arg_msg_q *)arg;
	mqd_t mq1 = thread_args->mq2;

	struct mq_attr attr;
    if (mq_getattr(mq1, &attr) == -1) {
        perror("mq_getattr error");
        return false; // Return false for failure
    }

	while(1)
	{
		struct rx_message* output_data = create_rx_message(attr.mq_msgsize);

		int read_eval = receive_message(mq1, output_data, 0);
		if (!read_eval) 
		{
			printf("error reading message\n");
			exit(1);
		}
		else
		{
			// printf("read audio_queue\n");
			printf("about to convert_to_audio\n");
			float * process_data = (float *)output_data->data;
			// for (int i = 0; i < process_size; i++)
			// {
			// 	printf(" %f\n", process_data[i]);
			// }
			// float * process_data = (float *)output_data->data;
			convert_to_audio(98500000.0, 0.0, 44100, process_size, process_data);
		}
	}
}

/*
		    thrd1		               thrd2      							thrd3
		|         |                |				|					|			|
        v         v                v                v                   v           v
   |antenna|     |- mq_trx-| --> |-     mq_work      -|		               |- APP -|

   |       | 	 |  IQ[]   |     |  	DOUBLE[]      |		               |       |
   |       | 	 |  IQ[]   |     |  	DOUBLE[]      |		               |       |
   |       | --> |  IQ[]   |     |  	DOUBLE[]      | - processed buf -> |       |

*/
int main() {
    char* uri;
    int result = get_uri(&uri);
	
	struct mq_attr attr;
	attr.mq_flags = 0;
	attr.mq_maxmsg = 10; // Maximum number of messages in the queue
	attr.mq_msgsize = 16384;  // Adjust the maximum message size
	attr.mq_curmsgs = 0;

	struct mq_attr attr2;
	attr2.mq_flags = 0;
	attr2.mq_maxmsg = 10; // Maximum number of messages in the queue
	attr2.mq_msgsize = 16384;  // Adjust the maximum message size
	attr2.mq_curmsgs = 0;

	struct mq_attr attr3;
	attr3.mq_flags = 0;
	attr3.mq_maxmsg = 10; // Maximum number of messages in the queue
	attr3.mq_msgsize = 16384;  // Adjust the maximum message size
	attr3.mq_curmsgs = 0;

	// struct mq_attr attr4;
	// attr3.mq_flags = 0;
	// attr3.mq_maxmsg = 10; // Maximum number of messages in the queue
	// attr3.mq_msgsize = 16384;  // Adjust the maximum message size
	// attr3.mq_curmsgs = 0;

	mqd_t mq_trx;
	check_message_queue_existence(rx_queue_name);
	mq_trx = create_message_queue(rx_queue_name, &attr);
    if (mq_trx == (mqd_t)-1) {
        perror("mq_open error");
		exit(1);
    }

	mqd_t mq_work;
	check_message_queue_existence(cuda_queue_name);
	mq_work = create_message_queue(cuda_queue_name, &attr2);
    if (mq_work == (mqd_t)-1) {
        perror("mq_open error");
		exit(1);
    }

	mqd_t mq_audio;
	check_message_queue_existence(audio_queue);
	mq_audio = create_message_queue(audio_queue, &attr3);
    if (mq_audio == (mqd_t)-1) {
        perror("mq_open error");
		exit(1);
    }

    if (result == 0) {
        printf("URI: %s\n", uri);
        IIO_ENSURE((ctx = iio_create_context_from_uri(uri)) && "No context");
    } else {
        // Handle the error accordingly.
		printf("error getting device from uri\n");
    }
	// enable transceiver configuration
	bool enable_success  = enable_and_set_params(MHZ(99.7), MHZ(2.084), MHZ(5), true, true, true, "hybrid", 32.0, NULL, true);
	// this is a kernel limit sysctl changes it
	int buffer_size = 16384;
	bool set_buffer_success =  set_buffer_size(buffer_size);
	printf("enable_and_set_params %d\n", enable_success);
	printf("set_buffer_size %d size: %d\n", set_buffer_success, buffer_size);

	// ----------------------------------------------------------------------------
	// execute threads
	// ----------------------------------------------------------------------------
	struct thrd_arg_msg_q thread_args_trx;
	struct thrd_arg_msg_q thread_args_trx2;
	struct thrd_arg_msg_q thread_args_work;
	struct thrd_arg_msg_q thread_args_work2;

	thread_args_trx.mq1  = mq_trx;
	thread_args_trx.mq2  = mq_work;
    
	thread_args_work.mq1  = mq_work;
	thread_args_work.mq2 = mq_audio; // -1 means not used

	pthread_t thread_id;
    int 	  result_thr;
    pthread_t thread_id2;
    int 	  result_thr2;
	pthread_t thread_id3;
    int 	  result_thr3;
	pthread_t thread_id4;
    int 	  result_thr4;

	// thread 1 // | ====== IQ data =========> {rx_queue}
	// thread 2 // 		{work queue}<=== process data ===== {rx_queue}

	// Create the thread and pass the threadArgs as an argument

	// fill IQ buffer
    result_thr = pthread_create(&thread_id, NULL, refill_buffer_thread_trx, &thread_args_trx);
    if (result_thr != 0) {
        perror("pthread_create error thread 1 send");
        return 1;
    }

	// read IQ buffer
    result_thr2 = pthread_create(&thread_id2, NULL, read_latest_message_trx, &thread_args_trx);
    if (result_thr2 != 0) {
        perror("pthread_create error thread 2 recv");
        return 1;
    }

	// read work buffer send to audio buffer
	result_thr3 = pthread_create(&thread_id3, NULL, read_latest_work, &thread_args_work);
    if (result_thr3 != 0) {
        perror("pthread_create error thread 2 recv");
        return 1;
    }

	// read audio buffer
	result_thr4 = pthread_create(&thread_id4, NULL, read_latest_audio, &thread_args_work);
    if (result_thr3 != 0) {
        perror("pthread_create error thread 2 recv");
        return 1;
    }

	pthread_join(thread_id, NULL);
    pthread_join(thread_id2, NULL);
	pthread_join(thread_id3, NULL);
	pthread_join(thread_id4, NULL);

	destroy_message_queue(rx_queue_name, mq_trx);
	free(uri); // Don't forget to free the memory after you are done using the URI.
    return 0;
}