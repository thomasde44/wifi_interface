#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <alsa/asoundlib.h>

unsigned int SAMPLE_RATE = 44100;   // Sample rate in Hz
#define BUFFER_SIZE 128    // Buffer size for audio samples

int main() {
    int err;
    snd_pcm_t *handle;
    short buffer[BUFFER_SIZE];  // Buffer to hold audio samples
    double phase = 0.0;         // Phase accumulator for sine wave
    const double frequency = 440.0;  // Frequency of the sine wave (A4 note in Hz)
    const double amplitude = 0.5 * 32767;  // Amplitude for 16-bit PCM

    // Open PCM device for playback
    if ((err = snd_pcm_open(&handle, "default", SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        printf("Playback open error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }

    // Set PCM parameters
    snd_pcm_hw_params_t *params;
    snd_pcm_hw_params_alloca(&params);
    snd_pcm_hw_params_any(handle, params);
    snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
    snd_pcm_hw_params_set_format(handle, params, SND_PCM_FORMAT_S16_LE);
    snd_pcm_hw_params_set_channels(handle, params, 1);
    snd_pcm_hw_params_set_rate_near(handle, params, &SAMPLE_RATE, 0);
    snd_pcm_hw_params(handle, params);

    while (1) {
        // Generate sine wave samples in the buffer
        for (int i = 0; i < BUFFER_SIZE; i++) {
            printf("%f\n", sin((float)(2.0) * M_PI * frequency * phase / SAMPLE_RATE));
            buffer[i] = (short)(amplitude * sin(2.0 * M_PI * frequency * phase / SAMPLE_RATE));
            phase += 1.0;  // Increment phase for the next sample
            if (phase >= SAMPLE_RATE) {
                phase -= SAMPLE_RATE;  // Reset phase if it exceeds the sample rate
            }
        }

        // Write the buffer to the PCM device
        if ((err = snd_pcm_writei(handle, buffer, BUFFER_SIZE)) != BUFFER_SIZE) {
            printf("Write to audio interface failed: %s\n", snd_strerror(err));
            snd_pcm_prepare(handle);
        }
    }

    snd_pcm_drain(handle);
    snd_pcm_close(handle);
    return 0;
}
