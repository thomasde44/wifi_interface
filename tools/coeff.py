import numpy as np
from scipy.signal import firwin

# Filter specifications
cutoff_freq = 12000000  # Cutoff frequency in Hz
sampling_freq = 98500000  # Sampling frequency in Hz
filter_length = 7  # Filter length (odd number)

# Compute the normalized cutoff frequency
nyquist_freq = 0.5 * sampling_freq
normalized_cutoff = cutoff_freq / nyquist_freq

# Design the filter using the windowing method
filter_coeffs = firwin(filter_length, normalized_cutoff, window='hamming')

# Print the filter coefficients
print(filter_coeffs)
