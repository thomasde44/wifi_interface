# import numpy as np
# import scipy.signal as signal

# # Filter specifications
# Fc = 44.1e3  # Cutoff frequency in Hz
# Tw = 1e6     # Transition width in Hz
# beta = 6.76  # Roll-off factor
# Fs = 2.084e6  # Sample rate

# # Calculate normalized cutoff frequency
# F_norm = Fc / Fs

# # Calculate filter length (N)
# N = 20#int(np.ceil(beta / Tw))

# # Design the ideal lowpass filter
# freq_response = np.array([1.0] + [0.0] * (N - 1))

# # Apply the Hamming window
# window = signal.windows.hamming(N)
# freq_response *= window

# # Compute the inverse Discrete Fourier Transform (IDFT)
# impulse_response = np.fft.ifft(freq_response)

# # Normalize the impulse response
# impulse_response /= np.sum(impulse_response)

# # Optional: Apply decimation scaling
# decimation_factor = 6
# impulse_response /= decimation_factor

# # Print the filter coefficients
# print(impulse_response)


coeff = "-0.0004	0.0002	-0.0002	0.0003	-0.0003	0.0004	-0.0004	0.0005	-0.0006	0.0007	-0.0008	0.0009	-0.0010	0.0011	-0.0012	0.0014	-0.0015	0.0017	-0.0018	0.0020	-0.0022	0.0024	-0.0026	0.0028	-0.0030	0.0032	-0.0035	0.0037	-0.0040	0.0042	-0.0045	0.0047	-0.0050	0.0052	-0.0055	0.0058	-0.0060	0.0063	-0.0066	0.0068	-0.0071	0.0073	-0.0076	0.0078	-0.0080	0.0082	-0.0085	0.0087	-0.0089	0.0090	-0.0092	0.0093	-0.0095	0.0096	-0.0097	0.0098	-0.0099	0.0099	-0.0100	0.0100	0.9900	0.0100	-0.0100	0.0099	-0.0099	0.0098	-0.0097	0.0096	-0.0095	0.0093	-0.0092	0.0090	-0.0089	0.0087	-0.0085	0.0082	-0.0080	0.0078	-0.0076	0.0073	-0.0071	0.0068	-0.0066	0.0063	-0.0060	0.0058	-0.0055	0.0052	-0.0050	0.0047	-0.0045	0.0042	-0.0040	0.0037	-0.0035	0.0032	-0.0030	0.0028	-0.0026	0.0024	-0.0022	0.0020	-0.0018	0.0017	-0.0015	0.0014 -0.0012	0.0011 -0.0010 0.0009 -0.0008 0.0007 -0.0006 0.0005 -0.0004 0.0004 -0.0003	0.0003 -0.0002 0.0002 -0.0004"
coeff = coeff.replace("\t", " ")
coeff_arr = coeff.split(" ")

res = []
cnt = 0
for i in coeff_arr:
    res.append(i.strip())
print(res)