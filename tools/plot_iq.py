import plotly.graph_objects as go
import plotly.offline as pyo
import numpy as np
import matplotlib.pyplot as plt


data = np.loadtxt('iq.txt')


real = data[:, 0]
imag = data[:, 1]

print(real)
print(imag)
complex_array = data[:, 0] + 1j * data[:, 1]
magnitude = np.abs(complex_array)
phase = np.angle(complex_array)
# print(magnitude)
# fig = go.Figure(data=go.Scatter(x=real, y=imag, mode='markers', marker=dict(size=10, color=magnitude, colorscale='viridis')))

fig = go.Figure()
fig.add_trace(go.Scatter(
    x=real,
    y=imag,
    mode='markers',
    marker=dict(
        size=10,
        color=magnitude,
        colorscale='Viridis',
        showscale=True,
        colorbar=dict(title='Magnitude')
    ),
    text=phase,
    hovertemplate="Magnitude: %{marker.color:.2f}<br>Phase: %{text:.2f}<extra></extra>"
))

fig.update_layout(
    title='Complex Numbers Plot',
    xaxis_title='Real',
    yaxis_title='Imaginary',
    hovermode='closest'
)

pyo.plot(fig, filename='mag.html', auto_open=False)
