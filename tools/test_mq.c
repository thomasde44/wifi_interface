#include "ru_device.h"


#define QUEUE_NAME "/my_message_queue"
#define MAX_MESSAGE_SIZE 20480


int main() {
    mqd_t mq;
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MAX_MESSAGE_SIZE;
    attr.mq_curmsgs = 0;

    mq_unlink(QUEUE_NAME); // Remove the queue if it exists

    mq = mq_open(QUEUE_NAME, O_CREAT | O_RDWR, S_IRWXU | S_IRWXG, &attr);
    if (mq == (mqd_t)-1) {
        perror("mq_open error");
        return 1;
    }
    int send_data[] = {1, 2, 3, 4, 5, 6};
    struct rx_message send_msg;
    send_msg.size = sizeof(send_data);
    send_msg.data = send_data;

    if (send_message(&send_msg, mq)) {
        printf("Message sent successfully.\n");
    }

    struct rx_message receive_msg;
    if (receive_message(mq, &receive_msg, 0)) {
        int* received_data = (int*)receive_msg.data;
        printf("Received Message: ");
        for (int i = 0; i < 6; i++) {
            printf("%d ", received_data[i]);
        }
        printf("\n");
        free(receive_msg.data);
    }

    mq_close(mq);
    mq_unlink(QUEUE_NAME); // Clean up the queue

    return 0;
}