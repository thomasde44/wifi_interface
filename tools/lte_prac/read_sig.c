#include <stdio.h>
#include <stdlib.h>

#define SAMPLE_SIZE 2    // Size of each sample in bytes (int16)

int main() {
    long fs = 30720000;
    double t0 = 0.0757;
    double T = 10e-3;

    // Calculate the offset and count in bytes
    long offset = (long)(t0 * fs * 4); // 9302016
    long count = (long)(T * fs * 2); // 614400

    // Open the file for reading
    FILE* file = fopen("LTE_uplink_847MHz_2022-01-30_30720ksps.sigmf-data", "rb");
    if (file == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    // Allocate memory for the array
    short* x = (short*)malloc(count);

    if (x == NULL) {
        printf("Error allocating memory.\n");
        fclose(file);
        return 1;
    }

    // Set the file position to the desired offset
    fseek(file, offset, SEEK_SET);

    // Read the data into the array
    size_t itemsRead = fread(x, SAMPLE_SIZE, count / SAMPLE_SIZE, file);

    // Check if the expected number of items were read
    if (itemsRead != count / SAMPLE_SIZE) {
        printf("Error reading file.\n");
        fclose(file);
        free(x);
        return 1;
    }

    int size = count / SAMPLE_SIZE;

    for (int i = 0; i < size; i++) {
        printf("%d\n", x[i]);
    }
    // Perform operations with the data stored in `x` array

    // Clean up resources
    fclose(file);
    free(x);

    return 0;
}
