#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <math.h>

#define MAX_SIZE 305008

// frame type 1 
// Tf = 307200*Ts = 10ms, 10 subframes 30720*Ts = 1ms
// each subframe consists of 10 subframes

// for subframes using 2.5khz, 7.5khz, 15khz sub carrier spacing (FDD)
// subframe i is defined as two slots 2i, 2i+1 of length Tslot = 15360Ts = 0.5ms

// for subframes using 1.25khz sub carrier spacing (TDD)
// subframe i is defined as one slot indexed as 2i of length Tslot = 30720Ts = 1ms


int main() {
    const char* filename = "LTE_uplink_847MHz_2022-01-30_30720ksps.sigmf-data";
    int fs = 30720000;
    double t0 = 0.0757;
    double T = 10e-3;
    double delta_f = 5.94e3;
    double delta_phi = 1.96;
    int Tu = 2048;
    int cp_len = 144;

    // Calculate the parameters
    int offset = (int)(t0 * fs * 4);
    int count = (int)(T * fs * 2);
    int data_size = count / 2; // Assuming the data is interleaved (real, imaginary)

    // Read the data from file
    int16_t* buffer = (int16_t*)malloc(count * sizeof(int16_t));
    FILE* file = fopen(filename, "rb");
    if (file == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    // jump to offset in the file
    fseek(file, offset, SEEK_SET);
    // begin reading from the offset
    fread(buffer, sizeof(int16_t), count, file);
    fclose(file);

    printf("start processing the data\n");
    // Process the data
    double complex* x = (double complex*)malloc(data_size * sizeof(double complex));
    for (int i = 0; i < data_size; i++) {
        // | --- 16bit --- | --- 16bit ---|
        // | --- real  --- | --- imag  ---|
        // | --- 2 * i --- | --- 2*i+1 ---|
        double real = (double)buffer[2 * i];
        double imaginary = (double)buffer[2 * i + 1];
        x[i] = (real + imaginary * I) / 2e4;
    }

    // Print the processed data
    // for (int i = 0; i < data_size; i++) {
    //     printf("x[%d]: %f %f\n",i,  creal(x[i]), cimag(x[i]));
    // }

    // Apply frequency and phase correction
    for (int i = 0; i < data_size; i++) {
        double phase = 2 * M_PI * delta_f / fs * i + delta_phi;
        x[i] *= cexp(I * phase);
    }

    printf("start correlation calc %d %d %d\n", MAX_SIZE, data_size, data_size - cp_len - Tu);
    // Calculate cp_corr
    double complex cp_corr[MAX_SIZE - cp_len - Tu];
    for (int j = 0; j < 300000; j++) {
        cp_corr[j] = 0.0 + 0.0 * I;
        for (int k = 0; k < cp_len; k++) {
            cp_corr[j] += x[j + k] * conj(x[j + Tu + k]);
            printf("%d %d %d %d\n", data_size - cp_len - Tu, j, k, cp_len);
        }
        
    }

    // // Print the processed data
    for (int i = 0; i < data_size; i++) {
        printf("x[%d]: %f + %f \n", i, creal(cp_corr[i]), cimag(cp_corr[i]));
    }

    // Clean up allocated memory
    free(buffer);
    free(x);

    return 0;
}
