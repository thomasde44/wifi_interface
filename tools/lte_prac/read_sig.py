import numpy as np
import matplotlib.pyplot as plt




# fs = 30720000

# # Read only the PUSCH slots of interest
# t0 = 0.0757
# T = 10e-3
# x = np.fromfile('LTE_uplink_847MHz_2022-01-30_30720ksps.sigmf-data', 'int16',
#                 offset=int(t0*fs*4), count=int(T*fs*2))





fs = 30720000

# Read only the PUSCH slots of interest
t0 = 0.0757
T = 10e-3
x = np.fromfile('LTE_uplink_847MHz_2022-01-30_30720ksps.sigmf-data', 'int16',
                offset=int(t0*fs*4), count=int(T*fs*2))

# Scale amplitude so that the PUSCH QPSK symbols are +/-1 +/-1j
x = (x[::2] + 1j*x[1::2])/2e4
# There is no uplink subcarrier at DC in the LTE UL. The central subcarriers
# are at +/-7.5 kHz. Here we shift up by ~6 kHz, in order to move the left central
# subcarrier to DC and take into account a CFO of ~1.5 kHz. The CFO is measured
# with the PUSCH and DMRS.
delta_f = 5.94e3
# Adjust phase offset (done with DMRS signal)
delta_phi = 1.96

y = np.fromfile('LTE_uplink_847MHz_2022-01-30_30720ksps.sigmf-data', 'int16')

# for i in zip(x, y):
#     print(i)

# for i in zip(x, x[::2], x[1::2]):
#     print(i)

# x = (x[::2] + 1j*x[1::2])/2e4

# print(len(x))

# for i in x:
#     print(i)



x *= np.exp(1j*(2*np.pi*delta_f/fs*np.arange(x.size) + delta_phi))


delta_f - 7.5e3



# The units here are samples
Tu = 2048 # useful time of a symbol
cp_len = 144  # 4.7 usecs, used in symbols 1-6 in a slot
cp_len2 = 160  # 5.2 usecs, used in symbol 0 in a slot



# Poor man's Schmidl & Cox: correlate the end of each symbol with its own cyclic prefix
cp_corr = np.empty(x.size - cp_len - Tu, 'complex64')
print('cp_corr', cp_corr.size)
for j in range(cp_corr.size):
    cp_corr[j] = np.sum(x[j:][:cp_len] * np.conjugate(x[j+Tu:][:cp_len]))

# print(x)
# print(x[j:][:cp_len])
# print("\n")
# print(x[j+Tu:][:cp_len])

for i in range(cp_corr.size):
    print(cp_corr[i])

# L = 20*Tu
# t = np.arange(L)/fs*1e6
# plt.plot(t, np.real(cp_corr[:L]))
# plt.plot(t, np.imag(cp_corr[:L]))
# plt.title('Correlation of $x[t:t+T_{cp}]$ and $x[t+T_u:t+T_u+T_{cp}]$')
# plt.xlabel('Time (us)')
# plt.ylabel('Amplitude')
# plt.legend(['I', 'Q'])

# plt.show()

