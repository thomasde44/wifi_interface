import matplotlib.pyplot as plt

# Read data from the file
data = []
with open("big.txt", "r") as file:
    for line in file:
        line = line.strip().split()
        data.append(int(line[0]))

# Plot the histogram
plt.hist(data, bins=10)  # Adjust the number of bins as needed
plt.xlabel("Value")
plt.ylabel("Frequency")
plt.title("Histogram of First Column")
plt.show()
