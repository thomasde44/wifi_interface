
% original_message is the original transport block.
% crc_polynomial is the coefficients of crc polynomials.
% This function generates a generator polynomial based on input crc
% polynomial. It then calculates the CRC check sum. This CRC checksum is
% then appended to the original transport block and returned as output of
% the function.
function tx_message=crc_24(original_message,crc_polynomial)
    message=original_message; 
    generator_polynomial = zeros(1, max(crc_polynomial) + 1);  % Initialize generator polynomial
    for i=1:length(crc_polynomial)
        generator_polynomial(1,crc_polynomial(1,i)+1)=1;    
    end
    generator_polynomial=flip(generator_polynomial); % Generator polynomial
    append_null=zeros(1,length(generator_polynomial)-1);
    message=[message append_null]; % Before division message is appended with zeros equal to order of CRC polynomial
    %Modulo-2 Division
    l_1=(length(message)-length(generator_polynomial));
    for i=1:l_1
        if (length(message)<length(generator_polynomial)) 
            break 
        end   
        R=xor(message(1,:),[generator_polynomial zeros(1,(length(message)-length(generator_polynomial))) ]); % one time xor done
        first = find(R, 1, 'first');
        disp(first)
        if ~isempty(first)
            message=R(first:length(R));
        else
            message = 0;
            break;
        end
    end
% Variable message will contain the CRC check sum
    tx_message = message;
end