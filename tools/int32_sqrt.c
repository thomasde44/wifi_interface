#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>

int32_t int32_sqrt(uint32_t i)
{
	int32_t n = 0;
	int bit = 0x8000;
	int delta_part_a = 0;
	int delta_part_b = bit*bit;

	while(bit >= 0)
	{
		int temp = i - (delta_part_a + delta_part_b);
		delta_part_a >>= 1;
		if (temp >= 0)
		{
			i -= temp;
			// delta_part_a += delta_part_b
			delta_part_a |= delta_part_b;
			// n += bit;
			n |= bit;
		}
		delta_part_b >>= 2;
		bit >>= 1;
	}
	return n;
}

int main()
{
	for (int32_t i = 0x7FFFFFFF; i > 0; i--)
	{
		if ((int)sqrt(i) != int32_sqrt(i))
		{
			printf("Error %i: %i != %i\n", i, (int)sqrt(i), int32_sqrt(i));
			return 0;
		}
	}
	printf("passed\n");
}

