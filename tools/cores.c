#include <stdio.h>
#include <unistd.h>

int main() {
    long numCores = sysconf(_SC_NPROCESSORS_ONLN);
    printf("Number of CPU cores: %ld\n", numCores);

    return 0;
}
