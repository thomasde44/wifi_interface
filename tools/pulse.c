#include <stdio.h>
#include <stdlib.h>
#include <math.h>

unsigned int SAMPLE_RATE = 44100;   // Sample rate in Hz
#define BUFFER_SIZE 1280000    // Buffer size for audio samples

int main() {
    FILE *file;
    short buffer[BUFFER_SIZE];  // Buffer to hold audio samples
    double phase = 0.0;         // Phase accumulator for sine wave
    const double frequency = 440.0;  // Frequency of the sine wave (A4 note in Hz)
    const double amplitude = 0.5 * 32767;  // Amplitude for 16-bit PCM

    // Open WAV file for writing
    file = fopen("output2.wav", "wb");
    if (!file) {
        printf("Error opening output file\n");
        exit(EXIT_FAILURE);
    }

    // Calculate data size (number of samples * number of channels * bytes per sample)
    const int dataSize = BUFFER_SIZE * 2;  // 2 bytes per sample (16-bit PCM)

    // Write WAV header (44 bytes for a standard 16-bit, mono WAV file)
    {
        unsigned char header[44] = {
            'R', 'I', 'F', 'F',  // Chunk ID
            0, 0, 0, 0,          // Chunk size (to be filled)
            'W', 'A', 'V', 'E',  // Format
            'f', 'm', 't', ' ',  // Subchunk1 ID
            16, 0, 0, 0,         // Subchunk1 size
            1, 0,               // Audio format (1 = PCM)
            1, 0,               // Number of channels (1 for mono)
            SAMPLE_RATE & 0xFF, (SAMPLE_RATE >> 8) & 0xFF, (SAMPLE_RATE >> 16) & 0xFF, (SAMPLE_RATE >> 24) & 0xFF,  // Sample rate
            (SAMPLE_RATE * 2) & 0xFF, ((SAMPLE_RATE * 2) >> 8) & 0xFF, ((SAMPLE_RATE * 2) >> 16) & 0xFF, ((SAMPLE_RATE * 2) >> 24) & 0xFF,  // Byte rate
            2, 0,               // Block align (2 bytes per sample)
            16, 0,              // Bits per sample (16-bit PCM)
            'd', 'a', 't', 'a', // Subchunk2 ID
            0, 0, 0, 0          // Subchunk2 size (to be filled)
        };
        fwrite(header, 1, sizeof(header), file);
    }

    // Generate sine wave samples in the buffer
    for (int i = 0; i < BUFFER_SIZE; i++) {
        buffer[i] = (short)(amplitude * sin(2.0 * M_PI * frequency * phase / SAMPLE_RATE));
        printf("%d\n", buffer[i]);
        phase += 1.0;  // Increment phase for the next sample
        if (phase >= SAMPLE_RATE) {
            phase -= SAMPLE_RATE;  // Reset phase if it exceeds the sample rate
        }
    }

    // Write the buffer to the WAV file
    fwrite(buffer, sizeof(short), BUFFER_SIZE, file);

    // Update WAV header with data size
    fseek(file, 4, SEEK_SET);
    const int chunkSize = 36 + dataSize;
    fwrite(&chunkSize, 4, 1, file);
    fseek(file, 40, SEEK_SET);
    fwrite(&dataSize, 4, 1, file);

    // Close the WAV file
    fclose(file);

    return 0;
}
