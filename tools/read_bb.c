#include <stdio.h>

int main() {
    FILE* file = fopen("uplink_50mhzbw.bb", "rb"); // Open the file in binary mode
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return 1;
    }

    // Seek to the beginning of the file (if necessary)
    // fseek(file, 0, SEEK_SET);

    // Read the file contents
    unsigned char buffer[4919673];  // Adjust the buffer size as per your needs
    // unsigned int buffer2[4919673];
    size_t bytesRead;
    while ((bytesRead = fread(buffer, sizeof(unsigned char), sizeof(buffer), file)) > 0) {
        // Process the data in the buffer
        // You can interpret and manipulate the baseband samples here
        unsigned int temp = 0;
        // Example: Print the bytes in hexadecimal format
        for (size_t i = 0; i < bytesRead; i+=2) {
            // temp = buffer[i] *
            printf("%d, %d\n", buffer[i], buffer[i+1]);
        }
    }

    fclose(file);
    return 0;
}



// #include <stdio.h>
// #include <string.h>

// #define MAX_BUFFER_SIZE 160062

// int main() {
//     FILE* file = fopen("baseband_samples_1ghz.bb", "rb"); // Open the file in binary mode
//     if (file == NULL) {
//         printf("Failed to open the file.\n");
//         return 1;
//     }

//     char headerString[16]; // Assuming the header string is not longer than 15 characters
//     size_t bytesRead = fread(headerString, sizeof(char), sizeof(headerString) - 1, file);
//     headerString[bytesRead] = '\0'; // Null-terminate the string

//     printf("Header: %s\n", headerString);

//     // Continue reading the rest of the file if needed
//     unsigned short buffer[MAX_BUFFER_SIZE / 2];
//     while ((bytesRead = fread(buffer, sizeof(unsigned short), sizeof(buffer), file)) > 0) {
//         int16_t numSamples = bytesRead / sizeof(unsigned short);
//         printf("%02X ", numSamples);
//         for (size_t i = 0; i < numSamples; i++) {
//             // Process each 16-bit sample in the buffer
//             unsigned short sample = buffer[i];
//             printf("%02lX ", buffer[i]);
//             // Perform your desired operations on the sample
//         }
//     }

//     fclose(file);
//     return 0;
// }